package com.droidjs.marketplaces.utils

object Constants {

    // Bottom menu values
    const val NAV_MARKETS = "markets"
    const val NAV_PRODUCE = "produce"
    const val NAV_LISTS = "lists"

    // Firebase Collections
    const val FIREBASE_USERS = "users"
    const val FIREBASE_MARKETS = "markets"
    const val FIREBASE_STANDS = "stand"
    const val FIREBASE_PRODUCTS = "product"

    // Analytics Events Custom Parameters
    const val ANALYTICS_PARAM_CONTENT_NAME = "content_name"

    // Content types for cntent type
    const val ANALYTICS_CONTENT_TYPE_PRODUCE = "produce"
    const val ANALYTICS_CONTENT_TYPE_STAND = "stand"
    const val ANALYTICS_CONTENT_TYPE_MARKET = "market"

    // Analytics User properties
    const val ANALYTICS_USER_NATIONALITY = "nationality"
    const val ANALYTICS_USER_MAIN_LANGUAGE = "main_language"

}