package com.droidjs.marketplaces.utils

import android.content.Context
import android.net.*

object ConnectionUtils {

    fun isConnected(context: Context): Boolean {
        val activeNetwork = getNetworkInfo(context)
        return activeNetwork?.isConnected == true
    }

    private fun getNetworkInfo(context: Context): NetworkInfo? {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return cm.activeNetworkInfo
    }
}

class ConnectionMonitor(private val callback: (Boolean) -> Unit): ConnectivityManager.NetworkCallback() {

    private val networkRequest = NetworkRequest.Builder()
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .build()

    fun enable(context: Context) {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        cm.registerNetworkCallback(networkRequest, this)
    }

    fun disable(context: Context) {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        cm.unregisterNetworkCallback(this)
    }

    override fun onAvailable(network: Network?) {
        callback(true)
    }

    override fun onLost(network: Network?) {
        callback(false)
    }
}