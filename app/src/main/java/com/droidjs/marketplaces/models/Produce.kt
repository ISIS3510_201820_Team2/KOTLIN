package com.droidjs.marketplaces.models

data class Produce(val uid: String = "",
                   val name: String = "",
                   val description: String = "",
                   val image: String = "",
                   val seasonMonths: List<Int> = emptyList())