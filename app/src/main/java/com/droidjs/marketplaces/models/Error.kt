package com.droidjs.marketplaces.models

sealed class Error(val message: String)

sealed class SignupError(message: String): Error(message)
class EmailError(message: String): SignupError(message)
class PasswordError(message: String): SignupError(message)
class ConfirmPasswordError(message: String): SignupError(message)
class AuthError(message: String): SignupError(message)

data class SignupErrors(
        var emailError: EmailError? = null,
        var passwordError: PasswordError? = null,
        var confirmPasswordError: ConfirmPasswordError? = null,
        var  authError: AuthError? = null
) {
    fun hasErrors() = emailError != null || passwordError != null || confirmPasswordError != null || authError != null
}
