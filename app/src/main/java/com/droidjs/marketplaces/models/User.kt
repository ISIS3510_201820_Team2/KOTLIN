package com.droidjs.marketplaces.models

data class User(
        val uid: String = "",
        val email: String = "",
        val firstName: String = "",
        val lastName: String = "",
        val nationality: String = "",
        val language: String = "",
        val favorites: Favorites = Favorites()
)

data class Favorites(
        val produces: List<String> = emptyList(),
        val stands: List<String> = emptyList()
)

