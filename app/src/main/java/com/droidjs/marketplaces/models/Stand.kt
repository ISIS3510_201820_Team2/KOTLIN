package com.droidjs.marketplaces.models

import com.google.firebase.firestore.DocumentReference

data class Stand( val uid:String,
        val market:DocumentReference,
        val name:String,
        val number:Int,
        val imageUrl:String,
        val behindTheStand:String,
        val produces:List<DocumentReference>,
        val sellers:List<String>
)


