package com.droidjs.marketplaces.models

data class Market(val uid: String, val name: String,
                  val lat: Double, val long: Double,
                  val stands: List<String>,
                  val summary: String, val location: String,
                  val openTimes: WeekTimes, val imageUrl: String
)

data class WeekTimes(
        val mon: String,
        val tue: String,
        val wed: String,
        val thu: String,
        val fri: String,
        val sat: String,
        val sun: String
)