package com.droidjs.marketplaces.views.market.info

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.droidjs.marketplaces.databinding.MarketInfoFragmentBinding

class MarketInfo : androidx.fragment.app.Fragment() {

    companion object {
        const val TAG = "MarketInfo"
        const val KEY_MARKET_UID = "market_uid"
        fun newInstance(marketUid: String): MarketInfo {
            val args = Bundle().apply {
                putString(KEY_MARKET_UID, marketUid)
            }
            return MarketInfo().apply { arguments = args }
        }
    }

    private lateinit var viewModel: MarketInfoViewModel
    private lateinit var binding: MarketInfoFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = MarketInfoFragmentBinding.inflate(inflater, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MarketInfoViewModel::class.java)
        binding.market = viewModel.market
        binding.open = viewModel.open
        binding.isLoading = viewModel.isLoading

        val uid = arguments?.getString(KEY_MARKET_UID).orEmpty()

        viewModel.searchMarket(uid)
    }

}
