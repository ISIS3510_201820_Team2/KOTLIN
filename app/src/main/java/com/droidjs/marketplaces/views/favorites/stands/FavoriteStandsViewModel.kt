package com.droidjs.marketplaces.views.favorites.stands

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.droidjs.marketplaces.firebase.models.toStand
import com.droidjs.marketplaces.firebase.models.toUser
import com.droidjs.marketplaces.firebase.standDoc
import com.droidjs.marketplaces.firebase.userDoc
import com.droidjs.marketplaces.models.Stand
import com.google.firebase.firestore.FirebaseFirestore

class FavoriteStandsViewModel: ViewModel() {

    private val firestore = FirebaseFirestore.getInstance()

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _isListVisible = MutableLiveData<Boolean>()
    val isListVisible: LiveData<Boolean> = _isListVisible

    private val _noItemsVisible = MutableLiveData<Boolean>()
    val noItemsVisible: LiveData<Boolean> = _noItemsVisible

    private val _listStands: MutableLiveData<List<Stand>> = MutableLiveData()
    val listStands: LiveData<List<Stand>> = _listStands

    fun searchStands(userId: String) {

        _isLoading.postValue(true)
        firestore.userDoc(userId)
                .get()
                .addOnCompleteListener { task ->
                    if(task.isSuccessful) {
                        val user = task.result?.toUser()
                        if(user != null) {
                            val favorites = user.favorites.stands
                            val stands: ArrayList<Stand> = arrayListOf()
                            for (standId: String in favorites) {
                                firestore.standDoc(standId).get().addOnCompleteListener { prodQuery ->
                                    if (prodQuery.isSuccessful) {
                                        val stand = prodQuery.result?.toStand()
                                        if(stand != null) stands.add(stand)
                                    }
                                    _listStands.postValue(stands)
                                }
                            }
                        }
                    }
                    _isLoading.postValue(false)
                }
    }

    fun setUserLoggedStatus(loggedIn: Boolean) {
        _isListVisible.postValue(loggedIn)
    }
}