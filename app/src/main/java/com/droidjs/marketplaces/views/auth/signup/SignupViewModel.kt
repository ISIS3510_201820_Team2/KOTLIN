package com.droidjs.marketplaces.views.auth.signup

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.droidjs.marketplaces.firebase.models.toFirebaseUser
import com.droidjs.marketplaces.firebase.models.toUser
import com.droidjs.marketplaces.firebase.userDoc
import com.droidjs.marketplaces.models.*
import com.droidjs.marketplaces.utils.Constants
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore

class SignupViewModel(application: Application) : AndroidViewModel(application) {

    private val _auth = FirebaseAuth.getInstance()
    private val _firestore = FirebaseFirestore.getInstance()
    private val _analytics = FirebaseAnalytics.getInstance(application)

    private val _user = MutableLiveData<FirebaseUser>()
    val user: LiveData<FirebaseUser> = _user

    private val _error = MutableLiveData<SignupErrors>()
    val error: LiveData<SignupErrors> = _error

    fun signUp(user: User, password: String, confirmPassword: String) {

        val errors = SignupErrors()
        if (user.email.isEmpty()) {
            errors.emailError = EmailError("The email can't be empty")
        }
        if (password.length < 8) {
            errors.passwordError = PasswordError("Password must be at least 8 characters")
        }
        if (password != confirmPassword) {
            errors.confirmPasswordError = ConfirmPasswordError("Password don't match")
        }
        if (errors.hasErrors()) {
            _error.postValue(errors)
        } else {
            _auth.createUserWithEmailAndPassword(user.email, password).addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val currentUser = _auth.currentUser
                    if (currentUser != null) {
                        _firestore.userDoc(currentUser.uid).set(user.toFirebaseUser())
                        logUserMetaDataAnalytics(user)
                        _user.postValue(currentUser)
                    }
                } else {
                    errors.authError = AuthError("Authentication failed")
                    _error.postValue(errors)
                }
            }
        }
    }

    private fun logUserMetaDataAnalytics(user: User) {
        val analytics = FirebaseAnalytics.getInstance(getApplication())
        analytics.setUserId(user.uid)
        analytics.setUserProperty(Constants.ANALYTICS_USER_NATIONALITY, user.nationality)
        analytics.setUserProperty(Constants.ANALYTICS_USER_MAIN_LANGUAGE, user.language)
    }
}