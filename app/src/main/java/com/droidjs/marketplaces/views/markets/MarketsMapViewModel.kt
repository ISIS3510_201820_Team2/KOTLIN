package com.droidjs.marketplaces.views.markets

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import android.location.Location
import android.os.Bundle
import android.util.Log
import com.droidjs.marketplaces.firebase.market
import com.droidjs.marketplaces.firebase.marketDoc
import com.droidjs.marketplaces.firebase.models.toMarket
import com.droidjs.marketplaces.models.Market
import com.droidjs.marketplaces.utils.Constants.ANALYTICS_CONTENT_TYPE_MARKET
import com.droidjs.marketplaces.utils.Event
import com.droidjs.marketplaces.views.marketList.MarketListViewModel
import com.google.android.gms.location.LocationServices
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.analytics.FirebaseAnalytics.Event.SELECT_CONTENT

class MarketsMapViewModel(application: Application): AndroidViewModel(application) {

    companion object {
        const val TAG = "MarketsMapViewModel"
    }

    private val firestore: FirebaseFirestore = FirebaseFirestore.getInstance()
    private val _analytics = FirebaseAnalytics.getInstance(application)

    private val _openMarket= MutableLiveData<Event<Market>>()

    val location = MutableLiveData<Location>()
    val requestPermission = MutableLiveData<Boolean>()
    val marketsList = MutableLiveData<List<Market>>()
    val selectedMarket = MutableLiveData<Market>()
    val isSearching = MutableLiveData<Boolean>().apply { postValue(false) }

    val openMarket: LiveData<Event<Market>> = _openMarket


    val getLastLocation: () -> Unit = {
        Log.d("MarketsMapViewModel", "Getting Location")
        try {
            LocationServices.getFusedLocationProviderClient(application)
                    .lastLocation.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    // Set the map's camera position to the current location of the device.
                    location.postValue( task.result)
                } else {
                    Log.d(TAG, "Current location is null. Using defaults.")
                    Log.e(TAG, "Exception: %s", task.exception)
                    requestPermissions()
                }
            }
        } catch (exc: SecurityException) {
            requestPermissions()
        }
    }

    private fun requestPermissions() {
        requestPermission.postValue(true)
    }

    fun searchMarkets() {
        isSearching.postValue(true)
        firestore.market().get().addOnCompleteListener { task ->
            if(task.isSuccessful) {
                val markets: List<Market> = task.result?.documents?.map { doc -> doc.toMarket() }.orEmpty()
                marketsList.postValue(markets)
            } else {
                Log.e(MarketListViewModel.TAG, "Error fetching markets")
            }
            isSearching.postValue(false)
        }
    }

    fun searchMarket(uid: String) {
        isSearching.postValue(true)
        firestore.marketDoc(uid).get().addOnCompleteListener { task ->
            if(task.isSuccessful) {
                val doc = task.result
                if (doc != null) {
                    Log.d(TAG, "Found market with id ${doc.id}")
                    selectedMarket.postValue(doc.toMarket())
                } else {
                    Log.e(TAG, "No Market with that ID")
                }
            } else {
                Log.e(MarketListViewModel.TAG, "Error fetching markets")
            }
            isSearching.postValue(false)
        }
    }

    fun clearMarket() {
        selectedMarket.postValue(null)
    }

    fun openMarketDetail() {
        selectedMarket.value?.let {
            val analyticsBundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.CONTENT_TYPE, ANALYTICS_CONTENT_TYPE_MARKET)
                putString(FirebaseAnalytics.Param.ITEM_ID, it.uid)
                putString(FirebaseAnalytics.Param.ITEM_NAME, it.name)
            }
            _analytics.logEvent(SELECT_CONTENT, analyticsBundle)
            _openMarket.postValue(Event(it))
        }
    }
}