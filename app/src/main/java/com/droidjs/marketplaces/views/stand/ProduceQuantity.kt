package com.droidjs.marketplaces.views.stand

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.droidjs.marketplaces.GlideApp
import com.droidjs.marketplaces.R
import kotlinx.android.synthetic.main.fragment_produce_quantity.imgProduceQuantity
import kotlinx.android.synthetic.main.fragment_produce_quantity.prNameQ
import kotlinx.android.synthetic.main.fragment_produce_quantity.qntButton
import kotlinx.android.synthetic.main.fragment_produce_quantity.quantityMenu
import kotlinx.android.synthetic.main.fragment_produce_quantity.quantityTxt

class ProduceQuantity: androidx.fragment.app.Fragment(){

    companion object {
        const val KEY_IMAGE = "image"
        const val KEY_NAME = "name"
        const val KEY_FROM_STAND_IMAGE = "started_from_stand_image"
        fun newInstance(image: String, name: String, fromStandImageUrl: String = ""): ProduceQuantity {
            val args = Bundle().apply {
                putString(KEY_IMAGE, image)
                putString(KEY_NAME, name)
                putString(KEY_FROM_STAND_IMAGE, fromStandImageUrl)
            }
            return ProduceQuantity().apply {
                arguments = args
            }
        }
    }

    var units: String = "Unidades"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_produce_quantity, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bundle = arguments
        if (bundle != null) {
            val image = bundle.getString(KEY_IMAGE, "")
            val name = bundle.getString(KEY_NAME, "")
            GlideApp.with(view).load(image).into(imgProduceQuantity)
            prNameQ.text = name

            val fromStand = bundle.getString(KEY_FROM_STAND_IMAGE, "")
            if(fromStand.isNotEmpty()) {
                qntButton.setOnClickListener {
                    val quantity = "${quantityTxt.text} $units"
                    val produceQ = ProducePrice.newInstance(image = image,
                            name = name, quantity = quantity, standImage = fromStand)
                    activity?.supportFragmentManager?.beginTransaction()
                            ?.addToBackStack(null)
                            ?.replace(R.id.container, produceQ)
                            ?.commit()
                }
            } else {
                qntButton.hide()
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val options = arrayOf("Units", "Pounds")

        quantityMenu.adapter = ArrayAdapter<String>(requireContext(), android.R.layout.simple_list_item_1, options)
        quantityMenu.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                units = "Units"
            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val spinnerTxt:String = options[position]
                units = if(spinnerTxt == "Units") {
                    "Unidades"
                } else {
                    "Libras"
                }
            }
        }


    }
}