package com.droidjs.marketplaces.views.auth.login

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import com.droidjs.marketplaces.R
import com.droidjs.marketplaces.utils.ConnectionMonitor
import com.droidjs.marketplaces.utils.ConnectionUtils
import com.droidjs.marketplaces.views.auth.signup.SignupActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    private lateinit var viewModel: LoginViewModel
    private lateinit var networkObserver: ConnectionMonitor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)

        login_btn.setOnClickListener {
            val email = email_txt.text.toString()
            val password = password_txt.text.toString()
            viewModel.login(email, password)
        }
        signup_btn.setOnClickListener { openSignUp() }
        setObservers()
        setNetworkObserver()
    }

    override fun onStart() {
        super.onStart()
        networkObserver.enable(this)
        if(!ConnectionUtils.isConnected(this)){
            login_btn.isEnabled = false
            showMissingConnection()
        }
        viewModel.getUser()
    }

    override fun onStop() {
        super.onStop()
        networkObserver.disable(this)
    }

    private fun setObservers() {
        viewModel.error.observe(this, Observer {errorEvent ->
            val message = errorEvent?.getContentIfNotHandled()
            if(message != null) {
                Snackbar.make(coordinator_view, message, Snackbar.LENGTH_LONG).show()
            }
        })
        viewModel.user.observe(this, Observer { user ->
            if (user != null) {
                finish()
            }
        })
    }

    private fun setNetworkObserver() {
        networkObserver = ConnectionMonitor { available ->
            login_btn.isEnabled = available
            if(!available) {
                showMissingConnection()
            }
        }
    }

    private fun showMissingConnection() {
        Snackbar.make(coordinator_view,
                R.string.message_login_no_connection, Snackbar.LENGTH_INDEFINITE).show()
    }

    private fun openSignUp() {
        val intent = Intent(this, SignupActivity::class.java)
        startActivity(intent)
    }
}
