package com.droidjs.marketplaces.views.stand

import android.content.Context
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.droidjs.marketplaces.GlideApp
import com.droidjs.marketplaces.R
import com.droidjs.marketplaces.firebase.models.toUser
import com.droidjs.marketplaces.firebase.userDoc
import com.droidjs.marketplaces.utils.ConnectionMonitor
import com.droidjs.marketplaces.utils.ConnectionUtils
import com.droidjs.marketplaces.utils.Constants
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.FirebaseAnalytics.Event
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_produce_details.dialogButton
import kotlinx.android.synthetic.main.fragment_produce_details.fav
import kotlinx.android.synthetic.main.fragment_produce_details.imgProduceDetails
import kotlinx.android.synthetic.main.fragment_produce_details.prDetails
import kotlinx.android.synthetic.main.fragment_produce_details.prName

class ProduceDetails: Fragment() {

    companion object {
        const val KEY_IMAGE = "image"
        const val KEY_NAME = "name"
        const val KEY_DESCRIPTION = "description"
        const val KEY_IMAGE_FROM_STAND = "image_from_stand"
        fun newInstance(uid:String, image: String, name: String, description: String, standImageURL: String = ""): ProduceDetails {
            val args = Bundle().apply {
                putString("image", image)
                putString("name", name)
                putString("description", description)
                putString("uid",uid)
                putString(KEY_IMAGE_FROM_STAND, standImageURL)
            }
            return ProduceDetails().apply {
                arguments = args
            }
        }
    }

    private val auth = FirebaseAuth.getInstance()
    private val firestore = FirebaseFirestore.getInstance()
    private lateinit var networkObserver: ConnectionMonitor

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        val uid = arguments?.getString("uid")
        val name = arguments?.getString("name")
        if( uid != null && name != null && context != null) {
            val analyticsBundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.CONTENT_TYPE, Constants.ANALYTICS_CONTENT_TYPE_PRODUCE)
                putString(FirebaseAnalytics.Param.ITEM_ID, uid)
                putString(FirebaseAnalytics.Param.ITEM_NAME, name)
            }
            FirebaseAnalytics.getInstance(context).logEvent(Event.SELECT_CONTENT, analyticsBundle)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_produce_details, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bundle = arguments
        if (bundle != null) {
            GlideApp.with(this)
                    .load(bundle.getString("image"))
                    .into(imgProduceDetails)
            prName.text = bundle.getString("name")
            prDetails.text = bundle.getString("description")

            val standImageURL = bundle.getString(KEY_IMAGE_FROM_STAND, "")
            if(standImageURL.isNotEmpty()) {
                dialogButton.setOnClickListener {
                    val produceQ = ProduceQuantity.newInstance(image = bundle.getString(KEY_IMAGE, ""),
                            name = bundle.getString(KEY_NAME, ""),
                            fromStandImageUrl = bundle.getString(KEY_IMAGE_FROM_STAND, ""))
                    activity?.supportFragmentManager?.beginTransaction()
                            ?.addToBackStack(null)
                            ?.replace(R.id.container, produceQ)
                            ?.commit()
                }
            } else {
                dialogButton.hide()
            }

            //favorite feature
            isFavorite()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //connection
        networkObserver = ConnectionMonitor { available ->
            if(!available) {
                showMissingConnection()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        context?.let {
            networkObserver.enable(it)
            if(!ConnectionUtils.isConnected(it)){
                showMissingConnection()
                fav.visibility = View.INVISIBLE

            }
            else{
                fav.visibility = if(auth.currentUser != null) View.VISIBLE else View.GONE
            }
        }
    }

    override fun onStop() {
        super.onStop()
        context?.let {
            networkObserver.disable(it)
        }
    }

    private fun isFavorite(){
        val currentUser = auth.currentUser
        if(currentUser != null) {
            fav.visibility = View.VISIBLE
            val docRef = firestore.userDoc(currentUser.uid)
            docRef.get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val user = task.result?.toUser()
                    if(user != null) {
                        val favorites = user.favorites.produces
                        val alreadyFavorited = favorites.contains(arguments?.getString("uid"))
                        setFavoriteButton(alreadyFavorited, currentUser.uid)
                    }
                }
            }
        } else {
            fav.visibility = View.GONE
        }
    }

    private fun addFavorite(userId: String){
            val docRef = firestore.userDoc(userId)
            docRef.get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val user = task.result?.toUser()
                    if (user != null) {
                        val favorites = user.favorites.produces
                        val newProduceId = arguments?.getString("uid").orEmpty()
                        if (!favorites.contains(newProduceId)) {
                            docRef.update(
                                    "favorites.produces", FieldValue.arrayUnion(newProduceId)
                            )
                            setFavoriteButton(true, userId)
                        }
                    }
                }
            }
    }

    private fun deleteFavorite(userId: String){
            val docRef = firestore.userDoc(userId)
            docRef.get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val user = task.result?.toUser()
                    if(user != null) {
                        val favorites = user.favorites.produces
                        val produceId = arguments?.getString("uid").orEmpty()
                        if (favorites.contains(produceId)) {
                            docRef.update(
                                    "favorites.produces", FieldValue.arrayRemove(produceId)
                            )
                            setFavoriteButton(false, userId)
                        }
                    }
                }
            }
    }

    private fun setFavoriteButton(alreadyFavorited: Boolean, userId: String) {
        if(alreadyFavorited) {
            fav?.setImageResource(R.drawable.ic_favorite_black_24dp)
            fav?.setOnClickListener { deleteFavorite(userId) }
        } else {
            fav?.setImageResource(R.drawable.ic_favorite_border_black_24dp)
            fav?.setOnClickListener { addFavorite(userId) }
        }
    }

    private fun showMissingConnection() {
        view?.let {
            Snackbar.make(it,
                    R.string.message_produce_calendar_no_connection, Snackbar.LENGTH_INDEFINITE).show()
        }
    }

    private fun showDialog(){
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("Offline connection")
        builder.setMessage("Your favorites list will update when there is an internet connection")
        builder.setPositiveButton("Ok"){dialog, which ->  
        }
        val dialog:AlertDialog = builder.create()
        dialog.show()
    }

}