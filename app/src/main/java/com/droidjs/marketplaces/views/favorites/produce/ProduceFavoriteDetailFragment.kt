package com.droidjs.marketplaces.views.favorites.produce

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.droidjs.marketplaces.R
import com.droidjs.marketplaces.databinding.FragmentFavoritesProduceListBinding
import com.droidjs.marketplaces.models.Produce
import com.droidjs.marketplaces.utils.ConnectionMonitor
import com.droidjs.marketplaces.utils.ConnectionUtils
import com.droidjs.marketplaces.views.produce.monthDetail.ProduceAdapter
import com.droidjs.marketplaces.views.stand.ProduceDetails
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_favorites_produce_list.*


class ProduceFavoriteDetailFragment: androidx.fragment.app.Fragment(), FirebaseAuth.AuthStateListener {

    companion object {
        const val KEY_USER_ID = "key_user_id"
        fun newInstance(userId: String): ProduceFavoriteDetailFragment {
            val bundle = Bundle().apply {
                putString(KEY_USER_ID, userId)
            }
            return ProduceFavoriteDetailFragment().apply {
                arguments = bundle
            }
        }
    }

    private val auth = FirebaseAuth.getInstance()

    private lateinit var viewmodel: ProduceFavoriteDetailViewModel
    private lateinit var binding: FragmentFavoritesProduceListBinding
    private var adapter: ProduceAdapter? = null
    private lateinit var networkObserver: ConnectionMonitor

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentFavoritesProduceListBinding.inflate(inflater, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewmodel = ViewModelProviders.of(this).get(ProduceFavoriteDetailViewModel::class.java)
        setListeners()

        binding.isLoading = viewmodel.isLoading
        binding.isListVisible = viewmodel.isListVisible

        setAdapter()

        networkObserver = ConnectionMonitor { available ->
            if(!available) {
                showMissingConnection()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        auth.addAuthStateListener(this)
        context?.let {
            networkObserver.enable(it)
            if(!ConnectionUtils.isConnected(it)){
                showMissingConnection()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        auth.removeAuthStateListener(this)
        context?.let {
            networkObserver.disable(it)
        }
    }

    override fun onAuthStateChanged(auth: FirebaseAuth) {
        val user = auth.currentUser
        if( user != null) {
            if (not_logged_message != null) not_logged_message.visibility = View.GONE
            viewmodel.searchProduce(user.uid)
        } else {
            viewmodel.setUserLoggedStatus(false)
            if (not_logged_message != null) not_logged_message.visibility = View.VISIBLE
        }
    }

    private fun setListeners() {
        viewmodel.listProduce.observe(this, Observer { listProduce ->
            if(listProduce != null) {
                adapter?.updateProduce(listProduce)
            }
        })
    }

    private fun setAdapter() {
        val userId = arguments?.getString(KEY_USER_ID).orEmpty()
        if(userId != "") {
            if (adapter == null) {
                adapter = ProduceAdapter(mutableListOf(), this::openProduce)
                viewmodel.searchProduce(userId)
            }
            produce_list.apply {
                layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
                adapter = this@ProduceFavoriteDetailFragment.adapter
            }
        } else {
            not_logged_message.visibility = View.VISIBLE
        }
    }

    private fun openProduce(produce: Produce) {
        val produceDetails = ProduceDetails.newInstance(produce.uid, produce.image, produce.name, produce.description)
        activity?.supportFragmentManager?.beginTransaction()
                ?.addToBackStack(null)
                ?.replace(R.id.container, produceDetails)
                ?.commit()
    }

    private fun showMissingConnection() {
        view?.let {
            Snackbar.make(it,
                    R.string.message_favorites_no_connection, Snackbar.LENGTH_INDEFINITE)
                    .show()
        }
    }
}