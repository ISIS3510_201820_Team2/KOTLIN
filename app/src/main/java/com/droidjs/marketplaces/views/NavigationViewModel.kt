package com.droidjs.marketplaces.views

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.droidjs.marketplaces.utils.Constants.NAV_MARKETS

class NavigationViewModel(application: Application): AndroidViewModel(application) {

    private var navState = NAV_MARKETS

    private val _selectedBottomNavigation = MutableLiveData<String>()
    val selectedBottomNavigation: LiveData<String> = _selectedBottomNavigation

    fun selectNav(goTo: String) {
        if (navState == goTo) return

        navState = goTo
        _selectedBottomNavigation.value = goTo
    }
}