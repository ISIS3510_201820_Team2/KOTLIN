package com.droidjs.marketplaces.views.marketList

import android.app.SearchManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.appcompat.widget.SearchView
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.droidjs.marketplaces.R
import com.droidjs.marketplaces.R.layout
import com.droidjs.marketplaces.models.Market
import com.droidjs.marketplaces.utils.Constants
import com.droidjs.marketplaces.views.market.MarketFragment
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.FirebaseAnalytics.Event
import kotlinx.android.synthetic.main.fragment_market_list.markets_list

class MarketListFragment : Fragment() {

    companion object {
        const val TAG = "MarketListFragment"
        const val KEY_QUERY = "query_str"
        fun newInstance(query: String): MarketListFragment {
            return MarketListFragment().apply {
                val bundle = Bundle()
                bundle.putString(KEY_QUERY, query)
                arguments = bundle
            }
        }
    }

    private lateinit var analytics: FirebaseAnalytics;
    private var adapter: MarketsAdapter? = null

    private lateinit var marketListViewModel: MarketListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layout.fragment_market_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = MarketsAdapter(mutableListOf(), this::openMarket)
        markets_list.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = this@MarketListFragment.adapter
        }

        marketListViewModel = ViewModelProviders.of(this).get(MarketListViewModel::class.java)
        setObservers()
        analytics = FirebaseAnalytics.getInstance(requireContext())
    }

    override fun onStart() {
        super.onStart()
        val query = arguments?.getString(KEY_QUERY).orEmpty()
        marketListViewModel.searchMarkets(query)
    }

    private fun setObservers() {
        marketListViewModel.marketsList.observe(this, Observer { markets ->
            if(markets != null) {
                Log.d(TAG, "Updating markets with " + markets.size + " markets")
                adapter?.updateMarkets(markets)
            }
        })
    }

    private fun openMarket(market: Market){
        val analyticsBundle = Bundle().apply {
            putString(FirebaseAnalytics.Param.CONTENT_TYPE, Constants.ANALYTICS_CONTENT_TYPE_MARKET)
            putString(FirebaseAnalytics.Param.ITEM_ID, market.uid)
            putString(FirebaseAnalytics.Param.ITEM_NAME, market.name)
        }
        analytics.logEvent(Event.SELECT_CONTENT, analyticsBundle)
        val standFragment = MarketFragment.newInstance(marketUid = market.uid,
                marketImgUrl = market.imageUrl)
        requireActivity().supportFragmentManager.beginTransaction()
                .addToBackStack(null)
                .replace(R.id.container, standFragment)
                .commit()
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.market_list_menu, menu)

        // Get the SearchView and set the searchable configuration
        val searchManager = activity?.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu.findItem(R.id.action_search).actionView as SearchView).apply {
            // Assumes current activity is the searchable activity
            setSearchableInfo(searchManager.getSearchableInfo(activity?.componentName))
            setIconifiedByDefault(false) // Do not iconify the widget; expand it by default
        }
    }
}
