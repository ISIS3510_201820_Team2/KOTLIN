package com.droidjs.marketplaces.views.favorites.stands

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.droidjs.marketplaces.R
import com.droidjs.marketplaces.databinding.FragmentFavoritesStandsBinding
import com.droidjs.marketplaces.utils.ConnectionMonitor
import com.droidjs.marketplaces.utils.ConnectionUtils
import com.droidjs.marketplaces.views.market.stands.StandsAdapter
import com.droidjs.marketplaces.views.stand.StandFragment
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_favorites_produce_list.not_logged_message
import kotlinx.android.synthetic.main.fragment_favorites_stands.*


class FavoriteStandsFragment: androidx.fragment.app.Fragment(), FirebaseAuth.AuthStateListener {

    companion object {
        const val KEY_USER_ID = "key_user_id"
        fun newInstance(userId: String): FavoriteStandsFragment {
            val bundle = Bundle().apply {
                putString(KEY_USER_ID, userId)
            }
            return FavoriteStandsFragment().apply {
                arguments = bundle
            }
        }
    }

    private val auth = FirebaseAuth.getInstance()

    private lateinit var viewmodel: FavoriteStandsViewModel
    private lateinit var binding: FragmentFavoritesStandsBinding
    private var adapter: StandsAdapter? = null
    private lateinit var networkObserver: ConnectionMonitor

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentFavoritesStandsBinding.inflate(inflater, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewmodel = ViewModelProviders.of(this).get(FavoriteStandsViewModel::class.java)
        setListeners()

        binding.isLoading = viewmodel.isLoading
        binding.noItemsVisible = viewmodel.noItemsVisible
        binding.isListVisible = viewmodel.isListVisible

        setAdapter()

        networkObserver = ConnectionMonitor { available ->
            if(!available) {
                showMissingConnection()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        auth.addAuthStateListener(this)
        context?.let {
            networkObserver.enable(it)
            if(!ConnectionUtils.isConnected(it)){
                showMissingConnection()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        auth.removeAuthStateListener(this)
        activity?.actionBar?.elevation = 8f
        context?.let {
            networkObserver.disable(it)
        }
    }

    override fun onAuthStateChanged(auth: FirebaseAuth) {
        val user = auth.currentUser
        if( user != null) {
            if (not_logged_message != null) not_logged_message.visibility = View.GONE
            viewmodel.searchStands(user.uid)
            viewmodel.setUserLoggedStatus(true)
        } else {
            if(not_logged_message != null) not_logged_message.visibility = View.VISIBLE
            viewmodel.setUserLoggedStatus(false)
        }
    }

    private fun setListeners() {
        viewmodel.listStands.observe(this, Observer { listStands ->
            if(listStands != null) {
                adapter?.updateStands(listStands)
            }
        })
    }

    private fun setAdapter() {
        val userId = arguments?.getString(KEY_USER_ID).orEmpty()
        if(userId != "") {
            if (adapter == null) {
                adapter = StandsAdapter(mutableListOf()) { standUid: String -> openStand(standUid)}
                viewmodel.searchStands(userId)
            }
            stands_list.apply {
                layoutManager = LinearLayoutManager(activity)
                adapter = this@FavoriteStandsFragment.adapter
            }
        } else {
            not_logged_message.visibility = View.VISIBLE
        }
    }

    private fun openStand(standUid:String) {
        val standFragment = StandFragment.newInstance(standUid)
        requireActivity().supportFragmentManager.beginTransaction()
                .addToBackStack(null)
                .replace(R.id.container, standFragment)
                .commit()
    }

    private fun showMissingConnection() {
        view?.let {
            Snackbar.make(it,
                    R.string.message_favorites_no_connection, Snackbar.LENGTH_INDEFINITE)
                    .show()
        }
    }
}