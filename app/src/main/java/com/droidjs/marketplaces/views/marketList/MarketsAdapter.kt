package com.droidjs.marketplaces.views.marketList

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.droidjs.marketplaces.databinding.MarketListItemBinding
import com.droidjs.marketplaces.models.Market

class MarketsAdapter(private var markets:MutableList<Market>, private val listener: (Market) -> Unit) :
        RecyclerView.Adapter<MarketsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val marketBinding = MarketListItemBinding.inflate(inflater, parent, false)
        return ViewHolder(marketBinding)
    }

    override fun getItemCount(): Int {
        return markets.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val market = markets[position]
        holder.bind(market, listener)
    }

    fun updateMarkets(newMarkets: List<Market>) {
        markets.clear()
        markets.addAll(newMarkets)
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: MarketListItemBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(market: Market, listener: (Market) -> Unit) {
            binding.market = market
            itemView.setOnClickListener { listener(market) }
            binding.executePendingBindings()
        }

    }
}