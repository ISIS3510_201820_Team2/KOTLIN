package com.droidjs.marketplaces.views.favorites

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.appcompat.app.AppCompatActivity
import android.view.*
import com.droidjs.marketplaces.R
import com.droidjs.marketplaces.utils.ConnectionMonitor
import com.droidjs.marketplaces.utils.ConnectionUtils
import com.droidjs.marketplaces.views.favorites.produce.ProduceFavoriteDetailFragment
import com.droidjs.marketplaces.views.favorites.stands.FavoriteStandsFragment
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_favorites.*
import java.lang.IllegalStateException

class FavoriteFragment : androidx.fragment.app.Fragment() {

    companion object {
        const val TAG = "FavoriteFragment"
        fun newInstance(): FavoriteFragment{
            return FavoriteFragment()
        }
    }

    private val auth = FirebaseAuth.getInstance()

    private lateinit var networkObserver: ConnectionMonitor
    private var adapter: FavoriteTabsPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favorites, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        networkObserver = ConnectionMonitor { available ->
            if(!available) {
                showMissingConnection()
            }
        }

        val userId = auth.currentUser?.uid.orEmpty()
        initPager(userId)
    }

    override fun onStart() {
        super.onStart()
        (activity as? AppCompatActivity?)?.supportActionBar?.elevation = 0f
        context?.let {
            networkObserver.enable(it)
            if(!ConnectionUtils.isConnected(it)){
                showMissingConnection()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        (activity as? AppCompatActivity?)?.supportActionBar?.elevation = 4f
        context?.let {
            networkObserver.disable(it)
        }
    }

    private inner class FavoriteTabsPagerAdapter(val userUid: String, fm: androidx.fragment.app.FragmentManager): androidx.fragment.app.FragmentStatePagerAdapter(fm) {
        override fun getItem(pos: Int): androidx.fragment.app.Fragment {
            return when(pos) {
                0 -> ProduceFavoriteDetailFragment.newInstance(userUid)
                1 -> FavoriteStandsFragment.newInstance(userUid)
                else -> throw IllegalStateException("Shouldn't try to load this position")
            }
        }

        override fun getCount(): Int = 2

        override fun getPageTitle(position: Int): CharSequence? {
            return when(position) {
                0 -> "PRODUCE"
                1 -> "STANDS"
                else -> "Invalid page"
            }
        }
    }

    private fun initPager(marketUid: String) {
        if(adapter == null) {
            adapter = FavoriteTabsPagerAdapter(marketUid,
                    childFragmentManager)
        }
        favorites_tabs_pager.adapter = adapter
    }

    private fun showMissingConnection() {
        view?.let {
            Snackbar.make(it,
                    R.string.message_favorites_no_connection, Snackbar.LENGTH_INDEFINITE).show()
        }
    }
}
