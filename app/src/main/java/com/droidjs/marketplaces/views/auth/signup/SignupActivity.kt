package com.droidjs.marketplaces.views.auth.signup

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import com.droidjs.marketplaces.R
import com.droidjs.marketplaces.models.SignupErrors
import com.droidjs.marketplaces.models.User
import com.droidjs.marketplaces.utils.ConnectionMonitor
import com.droidjs.marketplaces.utils.ConnectionUtils
import kotlinx.android.synthetic.main.activity_signup.*

class SignupActivity : AppCompatActivity() {

    private lateinit var viewModel: SignupViewModel
    private lateinit var networkObserver: ConnectionMonitor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        viewModel = ViewModelProviders.of(this).get(SignupViewModel::class.java)
        signup_btn.setOnClickListener { signup() }
        setUpObservers()
        setNetworkObserver()
    }

    override fun onStart() {
        super.onStart()
        networkObserver.enable(this)
        if(!ConnectionUtils.isConnected(this)){
            signup_btn.isEnabled = false
            showMissingConnection()
        }
    }

    override fun onStop() {
        super.onStop()
        networkObserver.disable(this)
    }

    private fun setUpObservers() {
        viewModel.user.observe(this, Observer {
            if(it != null) {
                finish()
            }
        })
        viewModel.error.observe(this, Observer { errors ->
            if(errors != null) processErrors(errors)
        })
    }

    private fun processErrors(errors: SignupErrors) {
        til_email.error = errors.emailError?.message
        til_password.error = errors.passwordError?.message
        til_confirm_password.error = errors.confirmPasswordError?.message

        errors.authError?.let {
            Snackbar.make(coordinator_layout, it.message, Snackbar.LENGTH_LONG).show()
        }
    }

    private fun signup() {
        val user = User(
                email = email_txt.text.toString(),
                firstName = firstname_txt.text.toString(),
                lastName = lastname_txt.text.toString(),
                nationality = nationality_txt.text.toString(),
                language = language_txt.text.toString()
        )
        viewModel.signUp(user, password_txt.text.toString(), confirm_password_txt.text.toString())
    }

    private fun setNetworkObserver() {
        networkObserver = ConnectionMonitor { available ->
            signup_btn.isEnabled = available
            if(!available) {
                showMissingConnection()
            }
        }
    }

    private fun showMissingConnection() {
        Snackbar.make(coordinator_layout,
                R.string.message_signup_no_connection, Snackbar.LENGTH_INDEFINITE).show()
    }
}
