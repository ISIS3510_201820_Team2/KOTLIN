package com.droidjs.marketplaces.views.produce

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.droidjs.marketplaces.R
import com.droidjs.marketplaces.utils.ConnectionMonitor
import com.droidjs.marketplaces.utils.ConnectionUtils
import com.droidjs.marketplaces.views.produce.monthDetail.ProduceCalendarMonthDetailFragment
import kotlinx.android.synthetic.main.fragment_produce_month_pager.*
import java.util.*

class ProduceMonthPagerFragment: androidx.fragment.app.Fragment() {

    companion object {
        const val TAG = "MonthPagerFragment"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_produce_month_pager, container, false)
    }

    private var adapter: MonthsPagerAdapter? = null
    private lateinit var networkObserver: ConnectionMonitor


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        networkObserver = ConnectionMonitor { available ->
            if(!available) {
                showMissingConnection()
            }
        }

        initPager()
    }

    override fun onStart() {
        super.onStart()
        context?.let {
            networkObserver.enable(it)
            if(!ConnectionUtils.isConnected(it)){
                showMissingConnection()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        context?.let {
            networkObserver.disable(it)
        }
    }

    private fun initPager() {
        if(adapter == null) {
            val cal = Calendar.getInstance()
            val month = cal.get(Calendar.MONTH)
            adapter = MonthsPagerAdapter(childFragmentManager)
            view_pager.adapter = adapter
            view_pager.setCurrentItem(month, true)
        } else {
            view_pager.adapter = adapter
        }
    }

    private inner class MonthsPagerAdapter(fm: androidx.fragment.app.FragmentManager): androidx.fragment.app.FragmentStatePagerAdapter(fm) {

        override fun getItem(pos: Int): androidx.fragment.app.Fragment {
            return ProduceCalendarMonthDetailFragment.newInstance(pos)
        }

        override fun getCount(): Int {
            return 12
        }
    }


    private fun showMissingConnection() {
        view?.let {
            Snackbar.make(it,
                    R.string.message_produce_calendar_no_connection, Snackbar.LENGTH_INDEFINITE)
                    .show()
        }
    }
}