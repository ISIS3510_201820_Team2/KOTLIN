package com.droidjs.marketplaces.views.camera

import android.app.Activity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.droidjs.marketplaces.databinding.ActivityCameraBinding
import com.google.zxing.Result
import kotlinx.android.synthetic.main.activity_camera.*
import me.dm7.barcodescanner.zxing.ZXingScannerView

class CameraActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {

    companion object {
        const val TAG = "CameraActivity"
        const val REQUEST_TYPE = "request_type"
        const val QR_REQUEST = CameraViewModel.QR_REQUEST
        const val KEY_RESULT = "key_result"
    }

    private lateinit var binding: ActivityCameraBinding
    private lateinit var viewModel: CameraViewModel

    private lateinit var requestType: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityCameraBinding.inflate(layoutInflater)
        setContentView(binding.root)

        requestType = intent.getStringExtra(REQUEST_TYPE) ?: ""
        if (requestType.isEmpty()) finish()

        viewModel = ViewModelProviders.of(this).get(CameraViewModel::class.java)
        viewModel.finishedProcessing.observe(this, Observer { processingEvent ->
            val result = processingEvent?.getContentIfNotHandled()
            if(result != null) {
                sendResult(result)
            }
        })

        binding.listener = viewModel
        camera.setResultHandler(this)
    }

    private fun sendResult(result: String) {

        val intent = Intent().apply {
            putExtra(KEY_RESULT, result)
        }
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onStart() {
        super.onStart()
        camera.startCamera()
    }

    override fun onStop() {
        super.onStop()
        camera.stopCamera()
    }

    override fun handleResult(result: Result?) {
        val id = result?.text.orEmpty()
        sendResult(id)
    }
}
