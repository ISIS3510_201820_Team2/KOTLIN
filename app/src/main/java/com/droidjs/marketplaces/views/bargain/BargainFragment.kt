package com.droidjs.marketplaces.views.bargain

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.droidjs.marketplaces.GlideApp

import com.droidjs.marketplaces.databinding.BargainFragmentBinding
import kotlinx.android.synthetic.main.bargain_fragment.*

class BargainFragment : androidx.fragment.app.Fragment() {

    companion object {
        const val KEY_INITIAL_PRICE = "init_value"
        const val KEY_STAND_IMAGE = "key_stand_image"
        fun newInstance(initialPrice: Double, standImage: String = "") = BargainFragment().apply {
            arguments = Bundle().apply {
                putDouble(KEY_INITIAL_PRICE, initialPrice)
                putString(KEY_STAND_IMAGE, standImage)
            }
        }
    }

    private lateinit var viewModel: BargainViewModel
    private lateinit var binding: BargainFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = BargainFragmentBinding.inflate(inflater, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bargain_merchant_agree_btn.isEnabled = false
        bargain_merchant_record_btn.isEnabled = false

        val imageUrl = arguments?.getString(KEY_STAND_IMAGE).orEmpty()

        GlideApp.with(this)
                .load(imageUrl)
                .placeholder(ColorDrawable(Color.GRAY))
                .circleCrop()
                .into(stand_image)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(BargainViewModel::class.java)
        binding.state = viewModel.data

        viewModel.data.observe(this, Observer { data ->
            if(data != null) handleData(data)
        })

        startSpeechToText()
        setListeners()
        viewModel.initView(arguments?.getDouble(KEY_INITIAL_PRICE) ?: 0.0)
    }

    private fun startSpeechToText() {
        val speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this.context)

        val merchantSpeechRegognitionIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        merchantSpeechRegognitionIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "es-ES")

        val userSpeechRecognitionIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)

        speechRecognizer.setRecognitionListener(object : RecognitionListener {
            override fun onRmsChanged(rmsdB: Float) {}

            override fun onBufferReceived(buffer: ByteArray?) {}

            override fun onPartialResults(partialResults: Bundle?) {}

            override fun onEvent(eventType: Int, params: Bundle?) {}

            override fun onBeginningOfSpeech() {}

            override fun onEndOfSpeech() {}

            override fun onError(error: Int) {}

            override fun onResults(bundle: Bundle?) {
                val matches = bundle?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                if (matches != null) {
                    val likeliestMatch = "[.,]".toRegex().replace(matches[0], "")
                    viewModel.setRecognizedText(likeliestMatch)
                    val match = """\s?$?(\d+)""".toRegex().find(likeliestMatch)
                    if (match != null) {
                        val price = match.groups[1]?.value.orEmpty().toDouble()
                        viewModel.makeOffer(price)
                    } else {
                    }
                }
            }

            override fun onReadyForSpeech(params: Bundle?) {}
        })

        bargain_merchant_record_btn.setOnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_UP -> {
                    speechRecognizer.stopListening()
                    bargain_merchant_offer_edit.hint = "You will see text here..."
                }
                MotionEvent.ACTION_DOWN -> {
                    speechRecognizer.startListening(merchantSpeechRegognitionIntent)
                    bargain_merchant_offer_edit.setText("")
                    bargain_merchant_offer_edit.hint = "Listening ...."
                }
            }
            false
        }

        bargain_merchant_record_btn.setOnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_UP -> {
                    speechRecognizer.stopListening()
                    bargain_merchant_offer_edit.hint = "You will see text here..."
                }
                MotionEvent.ACTION_DOWN -> {
                    speechRecognizer.startListening(merchantSpeechRegognitionIntent)
                    bargain_merchant_offer_edit.setText("")
                    bargain_merchant_offer_edit.hint = "Listening ...."
                }
            }
            false
        }

        bargain_user_record_btn.setOnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_UP -> {
                    speechRecognizer.stopListening()
                    bargain_user_offer_edit.hint = "You will see text here..."
                }
                MotionEvent.ACTION_DOWN -> {
                    speechRecognizer.startListening(userSpeechRecognitionIntent)
                    bargain_user_offer_edit.setText("")
                    bargain_user_offer_edit.hint = "Listening ...."
                }
            }
            false
        }
    }

    private fun setListeners() {
        bargain_merchant_agree_btn.setOnClickListener { viewModel.finishByMerchant()}
        bargain_user_agree_btn.setOnClickListener { viewModel.finishByUser() }
    }

    private fun handleData(data: BargainViewData) {
        enableButtons(data)
        setOfferText(data)
    }

    private fun enableButtons(data:BargainViewData) {
        bargain_user_record_btn.isEnabled = data.isUserTurn && !data.merchantBlocked
        bargain_user_agree_btn.isEnabled = data.isUserTurn && !data.userAgreed

        bargain_merchant_record_btn.isEnabled = !data.isUserTurn && !data.merchantBlocked
        bargain_merchant_agree_btn.isEnabled = !data.isUserTurn && !data.merchantBlocked
    }

    private fun setOfferText(data: BargainViewData) {
        bargain_merchant_offer_edit.setText(data.merchantOffer)
        bargain_user_offer_edit.setText(data.userOffer)
    }
}
