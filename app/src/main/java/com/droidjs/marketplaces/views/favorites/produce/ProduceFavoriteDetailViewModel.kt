package com.droidjs.marketplaces.views.favorites.produce

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.droidjs.marketplaces.firebase.models.toProduce
import com.droidjs.marketplaces.firebase.models.toUser
import com.droidjs.marketplaces.firebase.products
import com.droidjs.marketplaces.firebase.userDoc
import com.droidjs.marketplaces.models.Produce
import com.google.firebase.firestore.FirebaseFirestore

class ProduceFavoriteDetailViewModel: ViewModel() {

    private val firestore = FirebaseFirestore.getInstance()

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _isListVisible = MutableLiveData<Boolean>()
    val isListVisible: LiveData<Boolean> = _isListVisible

    private val _listProduce = MutableLiveData<List<Produce>>()
    val listProduce: LiveData<List<Produce>> = _listProduce

    fun searchProduce(userId: String) {

        _isLoading.postValue(true)
        firestore.userDoc(userId)
                .get()
                .addOnCompleteListener { task ->
                    if(task.isSuccessful) {
                        val user = task.result?.toUser()
                        if(user != null) {
                            val favorites = user.favorites.produces
                            val prods: ArrayList<Produce> = arrayListOf()
                            for (produce: String in favorites) {
                                firestore.products().document(produce).get().addOnCompleteListener { prodQuery ->
                                    if (prodQuery.isSuccessful) {
                                        val prDocument = prodQuery.result
                                        val pr = prDocument!!.toProduce()
                                        prods.add(pr)
                                    }
                                    _listProduce.postValue(prods)
                                }
                            }
                        }
                    }
                    _isListVisible.postValue(true)
                    _isLoading.postValue(false)
                }
    }

    fun setUserLoggedStatus(loggedIn: Boolean) {
        _isListVisible.postValue(loggedIn)
    }
}