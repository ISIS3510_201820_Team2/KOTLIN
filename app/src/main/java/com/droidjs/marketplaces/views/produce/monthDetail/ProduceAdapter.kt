package com.droidjs.marketplaces.views.produce.monthDetail

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.droidjs.marketplaces.databinding.ItemProduceListBinding
import com.droidjs.marketplaces.models.Produce

class ProduceAdapter(private var produce: MutableList<Produce>,
                     private val clickListener: (Produce) -> Unit): androidx.recyclerview.widget.RecyclerView.Adapter<ProduceAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val produceBinding = ItemProduceListBinding.inflate(inflater, parent, false)
        return ViewHolder(produceBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val produce = produce[position]
        holder.bind(produce, clickListener)
    }

    override fun getItemCount(): Int {
        return produce.size
    }

    fun updateProduce(newProduce: List<Produce>) {
        produce.clear()
        produce.addAll(newProduce)
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemProduceListBinding): androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {
        fun bind(produce: Produce, listener: (Produce) -> Unit) {
            binding.product = produce
            binding.executePendingBindings()
            itemView.setOnClickListener { listener(produce) }
        }
    }
}