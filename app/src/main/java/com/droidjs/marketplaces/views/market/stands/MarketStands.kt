package com.droidjs.marketplaces.views.market.stands

import android.app.Activity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.droidjs.marketplaces.R
import com.droidjs.marketplaces.databinding.MarketStandsFragmentBinding
import com.droidjs.marketplaces.views.camera.CameraActivity
import com.droidjs.marketplaces.views.stand.StandFragment
import kotlinx.android.synthetic.main.market_stands_fragment.*

class MarketStands : Fragment() {

    companion object {
        const val REQUEST_CODE_CAMERA = 11
        const val KEY_MARKET_UID = "market_uid"
        fun newInstance(marketUid: String): MarketStands {
            val args = Bundle().apply {
                putString(KEY_MARKET_UID, marketUid)
            }
            return MarketStands().apply { arguments = args }
        }
    }

    private var adapter: StandsAdapter? = null

    private lateinit var binding: MarketStandsFragmentBinding
    private lateinit var viewModel: MarketStandsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.market_stands_fragment, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MarketStandsViewModel::class.java)

        adapter = StandsAdapter(mutableListOf()) { standUid: String -> openStand(standUid)}
        stands_list.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = this@MarketStands.adapter
        }

        viewModel.activateCamera.observe(this, Observer { event ->
            if(event?.getContentIfNotHandled() == true) {
                val cameraIntent = Intent(context, CameraActivity::class.java)
                cameraIntent.putExtra(CameraActivity.REQUEST_TYPE, CameraActivity.QR_REQUEST)
                startActivityForResult(cameraIntent, REQUEST_CODE_CAMERA)
            }
        })
        viewModel.listStands.observe(viewLifecycleOwner, Observer { stands ->
            if(stands != null) {
                adapter?.updateStands(stands)
            }
        })

        viewModel.getStands(arguments?.getString(KEY_MARKET_UID).orEmpty())
        binding.listener = viewModel
        binding.isLoading = viewModel.isLoading
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            REQUEST_CODE_CAMERA -> if(resultCode == Activity.RESULT_OK) processCameraResult(data)
        }
    }

    private fun processCameraResult(data: Intent?) {
        val result = data?.getStringExtra(CameraActivity.KEY_RESULT) ?: ""
        if(result.isEmpty()) {
            Snackbar.make(view as View,  "Sorry problem reading QR", Snackbar.LENGTH_LONG).show()
        } else {
            openStand(result)
        }

    }

    private fun openStand(standUid:String) {
        val standFragment = StandFragment.newInstance(standUid)
        requireActivity().supportFragmentManager.beginTransaction()
                .addToBackStack(null)
                .replace(R.id.container, standFragment)
                .commit()
    }
}
