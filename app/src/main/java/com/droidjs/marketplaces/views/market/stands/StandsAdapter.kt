package com.droidjs.marketplaces.views.market.stands

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.droidjs.marketplaces.databinding.StandListItemBinding
import com.droidjs.marketplaces.models.Stand


class StandsAdapter(private var stands:MutableList<Stand>, private val clickListener: (String) -> Unit) :
        androidx.recyclerview.widget.RecyclerView.Adapter<StandsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val marketBinding = StandListItemBinding.inflate(inflater, parent, false)
        return ViewHolder(marketBinding)
    }

    override fun getItemCount(): Int {
        return stands.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val stand = stands[position]
        holder.bind(stand, clickListener)
    }

    fun updateStands(newStands: List<Stand>) {
        stands.clear()
        stands.addAll(newStands)
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: StandListItemBinding): androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {

        fun bind(stand: Stand, listener: (String) -> Unit) {
            binding.stand = stand
            binding.executePendingBindings()
            itemView.setOnClickListener { listener(stand.uid) }
        }

    }
}