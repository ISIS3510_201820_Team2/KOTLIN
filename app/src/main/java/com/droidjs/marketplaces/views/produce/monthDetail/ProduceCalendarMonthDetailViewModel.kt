package com.droidjs.marketplaces.views.produce.monthDetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.droidjs.marketplaces.firebase.models.toProduce
import com.droidjs.marketplaces.firebase.products
import com.droidjs.marketplaces.models.Produce
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

typealias MonthData = Pair<String, String>

class ProduceCalendarMonthDetailViewModel: ViewModel() {

    companion object {
        const val TAG  = "ProduceCalendarMonthDetailViewModel"
    }

    private val firestore = FirebaseFirestore.getInstance()

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _monthData = MutableLiveData<MonthData>()
    val monthData: LiveData<MonthData> = _monthData

    private val _listProduce = MutableLiveData<List<Produce>>()
    val listProduce: LiveData<List<Produce>> = _listProduce

    fun searchProduce(month: Int) {

        _isLoading.postValue(true)
        firestore.products()
                .whereArrayContains("seasonMonths", month)
                .get()
                .addOnCompleteListener { task ->
                    if(task.isSuccessful) {
                        task.result?.let { producesQuery ->
                            val onSeasonProduce = producesQuery.documents.map { it.toProduce() }
                            _listProduce.postValue(onSeasonProduce)
                        }
                    }
                    _isLoading.postValue(false)
                }
    }

    fun getMonthData(month: Int) {
        val monthNumber = String.format("%02d", month + 1)

        val calendar = Calendar.getInstance()
        calendar.set(Calendar.MONTH, month)
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        val monthName = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())
        _monthData.postValue(MonthData(monthNumber, monthName))
    }
}