package com.droidjs.marketplaces.views.stand

import android.content.Intent
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.speech.tts.TextToSpeech
import androidx.fragment.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.droidjs.marketplaces.GlideApp
import com.droidjs.marketplaces.R
import com.droidjs.marketplaces.views.bargain.BargainFragment
import kotlinx.android.synthetic.main.fragment_produce_price.bargain_start
import kotlinx.android.synthetic.main.fragment_produce_price.imgProducePrice
import kotlinx.android.synthetic.main.fragment_produce_price.insertAudio
import kotlinx.android.synthetic.main.fragment_produce_price.playAudio
import kotlinx.android.synthetic.main.fragment_produce_price.prNameP
import kotlinx.android.synthetic.main.fragment_produce_price.showSpeech
import kotlinx.android.synthetic.main.fragment_produce_price.textQuantity
import java.util.Locale

class ProducePrice: androidx.fragment.app.Fragment(),TextToSpeech.OnInitListener{

    companion object {
        const val TAG = "ProductPriceFragment"
        const val KEY_STAND_IMAGE = "key_stand_image"
        fun newInstance(image: String, name: String, quantity: String,
                        standImage: String = ""): ProducePrice {
            val args = Bundle().apply {
                putString("image", image)
                putString("name", name)
                putString("quantity",quantity)
                putString(KEY_STAND_IMAGE, standImage)
            }
            return ProducePrice().apply {
                arguments = args
            }
        }
    }

    private var tts: TextToSpeech? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_produce_price, container, false)
        tts = TextToSpeech(this.context,this)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bundle = arguments
        if (bundle != null) {
            GlideApp.with(view).load(bundle.getString("image")).into(imgProducePrice)
            prNameP.text = bundle.getString("name")
            textQuantity.text = bundle.getString("quantity")
        }
        showSpeech.hint = "You will see text here..."

        startSpeechToText()
        playAudio.setOnClickListener{speakOut()}
    }

    override fun onStart() {
        super.onStart()
        setBargainBtn()
    }

    private fun startSpeechToText(){
        val editText = showSpeech
        val speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this.context)
        val speechRecognizerIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "es-ES")

        speechRecognizer.setRecognitionListener(object: RecognitionListener{
            override fun onRmsChanged(rmsdB: Float) {}

            override fun onBufferReceived(buffer: ByteArray?) {}

            override fun onPartialResults(partialResults: Bundle?) {}

            override fun onEvent(eventType: Int, params: Bundle?) {}

            override fun onBeginningOfSpeech() {}

            override fun onEndOfSpeech() {}

            override fun onError(error: Int) {}

            override fun onResults(bundle: Bundle?) {
                val matches = bundle?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
                if (matches != null) {
                    val likeliestMatch = "[.,]".toRegex().replace(matches[0], "")
                    editText.setText(likeliestMatch)
                    setBargainBtn()
                }
            }

            override fun onReadyForSpeech(params: Bundle?) {}

        })

        insertAudio.setOnTouchListener { _, motionEvent ->
            when (motionEvent.action){
                MotionEvent.ACTION_UP -> {
                    speechRecognizer.stopListening()
                    editText.hint = "You will see text here..."
                }
                MotionEvent.ACTION_DOWN -> {
                    speechRecognizer.startListening(speechRecognizerIntent)
                    editText.setText("")
                    editText.hint = "Listening ...."
                }
            }
            false
        }
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {

            // set US English as language for tts
            val result = tts!!.setLanguage(Locale.US)


            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS","The Language specified is not supported!")
            } else {
            }

        } else {
            Log.e("TTS", "Initilization Failed!")
        }
    }

    private fun speakOut() {
        var text = showSpeech.text.toString()
        if(text.contains("$")){
            text = text.replace("$","") + "pesos"
        }
        tts!!.speak(text, TextToSpeech.QUEUE_FLUSH, null,"")
    }

    override fun onDestroy() {
        tts?.apply {
            stop()
            shutdown()
        }
        super.onDestroy()
    }

    private fun openBargain(price: Double) {
        val standImage = arguments?.getString(KEY_STAND_IMAGE).orEmpty()
        val bargainFrag = BargainFragment.newInstance(price, standImage)
        activity?.supportFragmentManager?.beginTransaction()
                ?.addToBackStack(null)
                ?.replace(R.id.container,bargainFrag)
                ?.commit()
    }

    private fun setBargainBtn() {
        val currentText = showSpeech.text.toString()
        val match = """\s?$?(\d+)""".toRegex().find(currentText)
        if(match != null) {
            val price = match.groups[1]?.value.orEmpty().toDouble()
            bargain_start.visibility = View.VISIBLE
            bargain_start.setOnClickListener { openBargain(price) }
        } else {
            bargain_start.visibility = View.INVISIBLE
        }
    }
}
