package com.droidjs.marketplaces.views.market

import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.droidjs.marketplaces.R
import com.droidjs.marketplaces.databinding.MarketFragmentBinding
import com.droidjs.marketplaces.utils.ConnectionMonitor
import com.droidjs.marketplaces.utils.ConnectionUtils
import com.droidjs.marketplaces.utils.load
import com.droidjs.marketplaces.views.NavigationViewModel
import com.droidjs.marketplaces.views.market.info.MarketInfo
import com.droidjs.marketplaces.views.market.stands.MarketStands
import kotlinx.android.synthetic.main.market_fragment.market_detail_img
import kotlinx.android.synthetic.main.market_fragment.market_tabs_pager

class MarketFragment : androidx.fragment.app.Fragment() {

    companion object {
        const val TAG = "MarketFragment"
        const val NAV_STATE = "market_detail"
        const val KEY_MARKET_UID = "market_uid"
        const val KEY_MARKET_IMG = "market_imageUrl"
        fun newInstance(marketUid: String, marketImgUrl: String): MarketFragment {
            val args = Bundle().apply {
                putString(KEY_MARKET_UID, marketUid)
                putString(KEY_MARKET_IMG, marketImgUrl)
            }
            val fragment = MarketFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var viewModel: MarketViewModel
    private lateinit var navViewModel: NavigationViewModel
    private lateinit var networkObserver: ConnectionMonitor

    private lateinit var binding: MarketFragmentBinding
    private var adapter: MarketTabsPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        postponeEnterTransition()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.market_fragment, container, false)
        binding.setLifecycleOwner(this)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        navViewModel = ViewModelProviders.of(requireActivity()).get(NavigationViewModel::class.java)
        viewModel = ViewModelProviders.of(requireActivity()).get(MarketViewModel::class.java)

        binding.market = viewModel.market

        networkObserver = ConnectionMonitor { available ->
            if(!available) {
                showMissingConnection()
            }
        }

        arguments?.getString(KEY_MARKET_IMG).orEmpty().let {
            if(it.isEmpty()) startPostponedEnterTransition()
            else {
                market_detail_img.load(it){ startPostponedEnterTransition() }
            }
        }

        val uid = savedInstanceState?.getString(KEY_MARKET_UID) ?: arguments?.getString(KEY_MARKET_UID).orEmpty()
        initPager(uid)
    }

    override fun onStart() {
        super.onStart()
        context?.let {
            networkObserver.enable(it)
            if(!ConnectionUtils.isConnected(it)){
                showMissingConnection()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        context?.let {
            networkObserver.disable(it)
        }
    }

    private inner class MarketTabsPagerAdapter(val marketUid: String, fm: androidx.fragment.app.FragmentManager): androidx.fragment.app.FragmentStatePagerAdapter(fm) {
        override fun getItem(pos: Int): androidx.fragment.app.Fragment {
            return when(pos) {
                0 -> MarketInfo.newInstance(marketUid)
                1 -> MarketStands.newInstance(marketUid)
                else -> throw IllegalStateException("Shouldn't try to load this position")
            }
        }

        override fun getCount(): Int = 2

        override fun getPageTitle(position: Int): CharSequence? {
            return when(position) {
                0 -> "INFO"
                1 -> "STANDS"
                else -> "Invalid page"
            }
        }
    }

    private fun initPager(marketUid: String) {
        if(adapter == null) {
            adapter = MarketTabsPagerAdapter(marketUid,
                    childFragmentManager)
        }
        market_tabs_pager.adapter = adapter
    }

    private fun showMissingConnection() {
        view?.let {
            Snackbar.make(it,
                    R.string.message_market_no_connection, Snackbar.LENGTH_INDEFINITE).show()
        }
    }
}
