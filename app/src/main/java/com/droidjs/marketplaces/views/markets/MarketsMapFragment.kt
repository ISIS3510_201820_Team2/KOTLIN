package com.droidjs.marketplaces.views.markets

import android.Manifest
import android.app.SearchManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.content.pm.PackageManager
import androidx.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.transition.Fade
import androidx.transition.TransitionInflater
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.appcompat.widget.SearchView
import android.util.Log
import android.view.*
import com.droidjs.marketplaces.GlideApp
import com.droidjs.marketplaces.R
import com.droidjs.marketplaces.databinding.FragmentMarketMapBinding
import com.droidjs.marketplaces.utils.ConnectionUtils
import com.droidjs.marketplaces.views.market.MarketFragment
import com.google.android.gms.maps.*

import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_market_map.*


class MarketsMapFragment : androidx.fragment.app.Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    companion object {
        const val TAG = "MarketsMapFragment"
        const val PERMISSION_REQUEST_LOCATION = 77
        const val KEY_SELECTED_MARKET = "selected_market"
        const val KEY_CAMERA_POS = "camera_pos"
    }

    private var mMap: GoogleMap? = null
    private var cameraPosition: CameraPosition? = null
    private lateinit var binding: FragmentMarketMapBinding
    private lateinit var marketsMapViewModel: MarketsMapViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_market_map, container, false)
        binding.setLifecycleOwner(this)

        marketsMapViewModel = ViewModelProviders.of(this).get(MarketsMapViewModel::class.java)
        setObservers()

        cameraPosition = savedInstanceState?.getParcelable(KEY_CAMERA_POS)
        if (savedInstanceState != null) {
            val uid = savedInstanceState.getString("selected_market", "")
            if (uid != "") marketsMapViewModel.searchMarket(uid)
        }

        binding.market = marketsMapViewModel.selectedMarket
        binding.listener = marketsMapViewModel

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        if(mMap == null) {
            val mapFragment = SupportMapFragment.newInstance()
            mapFragment.getMapAsync(this)
            childFragmentManager.beginTransaction().replace(R.id.map, mapFragment).commit()
            marketsMapViewModel.getLastLocation()
        }
    }

    override fun onStart() {
        super.onStart()
        val isConnected = ConnectionUtils.isConnected(requireContext())
        if(!isConnected) {
            Snackbar.make(view as View, R.string.message_map_no_connection, Snackbar.LENGTH_LONG).show()
        }
        cameraPosition?.let { mMap?.moveCamera(CameraUpdateFactory.newCameraPosition(it)) }
        marketsMapViewModel.searchMarkets()
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater){
        menuInflater.inflate(R.menu.markets_menu, menu)

        // Get the SearchView and set the searchable configuration
        val searchManager = activity?.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu.findItem(R.id.action_search).actionView as SearchView).apply {
            // Assumes current activity is the searchable activity
            setSearchableInfo(searchManager.getSearchableInfo(requireActivity().componentName))
            setIconifiedByDefault(false) // Do not iconify the widget; expand it by default
        }
    }

    private fun setObservers() {
        marketsMapViewModel.location.observe(this, Observer { location ->
            if (location != null) {
                val latLng = LatLng(location.latitude, location.longitude)
                mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
            }
        })
        marketsMapViewModel.requestPermission.observe(this, Observer { request ->
            if(request == true) {
                requestLocationPermissions()
            }
        })
        marketsMapViewModel.marketsList.observe(this, Observer { markets ->
            markets?.forEach { market ->
                val pos = LatLng(market.lat, market.long)
                val marker = mMap?.addMarker(MarkerOptions()
                        .position(pos)
                        .title(market.name))
                marker?.tag = market.uid
            }
        })
        marketsMapViewModel.selectedMarket.observe(this, Observer { market ->
            if(market != null) {
                GlideApp.with(this)
                        .load(market.imageUrl)
                        .into(market_image)
            }
        })
        marketsMapViewModel.openMarket.observe(this, Observer { event ->
            event?.getContentIfNotHandled()?.let { market ->
                val marketFragment = MarketFragment.newInstance(market.uid, market.imageUrl)
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    marketFragment.enterTransition = Fade()
                    marketFragment.sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
                    marketFragment.sharedElementReturnTransition = TransitionInflater.from(context).inflateTransition(R.transition.inverse_move)
                }
                requireActivity().supportFragmentManager.beginTransaction()
                        .addToBackStack(null)
                        .addSharedElement(market_image, "marketCard")
                        .replace(R.id.container, marketFragment)
                        .commit()
            }
        })
    }

    private fun requestLocationPermissions() {
        requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.CAMERA,Manifest.permission.RECORD_AUDIO),
                PERMISSION_REQUEST_LOCATION)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        if(cameraPosition != null) {
            mMap?.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        } else {
            marketsMapViewModel.getLastLocation()
        }
        setUpMap(mMap)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_LOCATION -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    try {
                        marketsMapViewModel.getLastLocation()
                        setUpMap(mMap)
                    } catch (e: SecurityException) {
                        Log.e("Exception: %s", e.message)
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }
            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }

    private fun setUpMap(map: GoogleMap?)  {
        if(ActivityCompat.checkSelfPermission(requireContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            map!!.isMyLocationEnabled = true
            map.uiSettings.isMyLocationButtonEnabled = true
            map.uiSettings.isZoomControlsEnabled = true
            map.setOnMarkerClickListener(this)
            map.setOnMapClickListener { _ -> marketsMapViewModel.clearMarket() }
        }
    }


    override fun onMarkerClick(marker: Marker): Boolean {
        val uid = marker.tag as String
        marketsMapViewModel.searchMarket(uid)
        Log.d(TAG, "Updating view model with market")
        return false
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(KEY_SELECTED_MARKET, marketsMapViewModel.selectedMarket.value?.uid.orEmpty())
        if (mMap!=null) {
            outState.putParcelable(KEY_CAMERA_POS, mMap?.cameraPosition)
        }
        super.onSaveInstanceState(outState)
    }
}
