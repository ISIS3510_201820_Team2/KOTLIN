package com.droidjs.marketplaces.views.produce.monthDetail

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.droidjs.marketplaces.R
import com.droidjs.marketplaces.databinding.FragmentCalendarProduceDetailBinding
import com.droidjs.marketplaces.models.Produce
import com.droidjs.marketplaces.views.stand.ProduceDetails
import kotlinx.android.synthetic.main.fragment_calendar_produce_detail.*

class ProduceCalendarMonthDetailFragment: androidx.fragment.app.Fragment() {

    companion object {
        const val KEY_DISPLAY_MONTH = "key_display_month"
        fun newInstance(month: Int): ProduceCalendarMonthDetailFragment {
            val bundle = Bundle().apply {
                putInt(KEY_DISPLAY_MONTH, month)
            }
            return ProduceCalendarMonthDetailFragment().apply {
                arguments = bundle
            }
        }
    }

    private lateinit var viewmodel: ProduceCalendarMonthDetailViewModel
    private lateinit var binding: FragmentCalendarProduceDetailBinding
    private var adapter: ProduceAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentCalendarProduceDetailBinding.inflate(inflater, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewmodel = ViewModelProviders.of(this).get(ProduceCalendarMonthDetailViewModel::class.java)
        setListeners()

        binding.isLoading = viewmodel.isLoading
        binding.monthData = viewmodel.monthData

        val month = arguments?.getInt(KEY_DISPLAY_MONTH) ?: 0
        viewmodel.getMonthData(month)

        if(adapter == null) {
            adapter = ProduceAdapter(mutableListOf(), this::openProduce)
            viewmodel.searchProduce(month)
        }
        produce_list.apply {
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(activity)
            adapter = this@ProduceCalendarMonthDetailFragment.adapter
        }

    }

    private fun setListeners() {
        viewmodel.listProduce.observe(this, Observer { listProduce ->
            if(listProduce != null) {
                adapter?.updateProduce(listProduce)
            }
        })
    }

    private fun openProduce(produce: Produce) {
        val produceDetails = ProduceDetails.newInstance(produce.uid, produce.image, produce.name, produce.description)
        activity?.supportFragmentManager?.beginTransaction()
                ?.addToBackStack(null)
                ?.replace(R.id.container, produceDetails)
                ?.commit()
    }
}