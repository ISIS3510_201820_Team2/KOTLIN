package com.droidjs.marketplaces.views.market

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Log
import com.droidjs.marketplaces.firebase.marketDoc
import com.droidjs.marketplaces.firebase.models.toMarket
import com.droidjs.marketplaces.models.Market
import com.google.firebase.firestore.FirebaseFirestore

class MarketViewModel : ViewModel() {

    companion object {
        const val TAG  = "MarketViewModel"
    }

    private val firestore = FirebaseFirestore.getInstance()
    private val _market = MutableLiveData<Market>()
    private val isSearching = MutableLiveData<Boolean>()

    val market: LiveData<Market> = _market
    fun isSearching(): LiveData<Boolean> = isSearching

    fun searchMarket(uid: String) {
        if(uid == "") {
            Log.e(TAG, "Invalid search")
            return
        }
        isSearching.postValue(true)
        firestore.marketDoc(uid).get().addOnCompleteListener { task ->
            if(task.isSuccessful) {
                val doc = task.result
                if (doc != null) {
                    Log.d(TAG, "Found _market with id ${doc.id}")
                    _market.postValue(doc.toMarket())
                } else {
                    Log.e(TAG, "No Market with that ID")
                }
            } else {
                Log.e(TAG, "Error fetching markets")
            }
            isSearching.postValue(false)
        }
    }
}
