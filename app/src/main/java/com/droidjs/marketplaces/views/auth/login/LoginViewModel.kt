package com.droidjs.marketplaces.views.auth.login

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import com.droidjs.marketplaces.firebase.models.toUser
import com.droidjs.marketplaces.firebase.userDoc
import com.droidjs.marketplaces.utils.Constants.ANALYTICS_USER_MAIN_LANGUAGE
import com.droidjs.marketplaces.utils.Constants.ANALYTICS_USER_NATIONALITY
import com.droidjs.marketplaces.utils.Event
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore

class LoginViewModel(application: Application): AndroidViewModel(application) {

    companion object {
        const val TAG = "LoginViewModel"
    }

    private val _auth = FirebaseAuth.getInstance()
    private val _firestore = FirebaseFirestore.getInstance()

    private val _error = MutableLiveData<Event<String>>()
    val error: LiveData<Event<String>> = _error

    private val _user = MutableLiveData<FirebaseUser>()
    val user: LiveData<FirebaseUser> = _user

    fun getUser() {
        _user.postValue(_auth.currentUser)
    }

    fun login(email: String, password: String) {
        if (email.isEmpty() || password.isEmpty()) {
            _error.postValue(Event("You must fill email and password"))
        } else {
            _auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { loginTask ->
                if (loginTask.isSuccessful) {
                    _auth.currentUser?.let {
                        logUserMetaDataAnalytics(it.uid)
                    }
                    _user.postValue(_auth.currentUser)
                } else {
                    Log.w(TAG, "signInWithEmail:failure", loginTask.exception)
                    _error.postValue(Event("Authentication Failed"))
                }
            }
        }
    }

    private fun logUserMetaDataAnalytics(userId: String) {
        _firestore.userDoc(userId).get().addOnCompleteListener { task ->
            if(task.isSuccessful) {
                task.result?.let { userDoc ->
                    val user = userDoc.toUser()
                    Log.d("User", user.toString())
                    val analytics = FirebaseAnalytics.getInstance(getApplication())
                    analytics.setUserId(userId)
                    analytics.setUserProperty(ANALYTICS_USER_NATIONALITY, user.nationality)
                    analytics.setUserProperty(ANALYTICS_USER_MAIN_LANGUAGE, user.language)
                }
            }
        }
    }
}