package com.droidjs.marketplaces.views.market.stands

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Log
import com.droidjs.marketplaces.firebase.models.toStand
import com.droidjs.marketplaces.firebase.stand
import com.droidjs.marketplaces.models.Stand
import com.droidjs.marketplaces.utils.Event
import com.google.firebase.firestore.FirebaseFirestore

class MarketStandsViewModel: ViewModel() {

    companion object {
        const val TAG = "MarketStandsViewModel"
    }

    private val firestore = FirebaseFirestore.getInstance()

    private val _activateCamera = MutableLiveData<Event<Boolean>>()
    val activateCamera: LiveData<Event<Boolean>> = _activateCamera

    private val _listStands = MutableLiveData<List<Stand>>()
    val listStands: LiveData<List<Stand>> = _listStands

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    fun onClickFAB() {
        _activateCamera.postValue(Event(true))
    }

    fun getStands(marketId: String) {
        if (marketId.isEmpty()) {
            Log.e(TAG, "Can't query for an empty string")
        }
        val marketRef = firestore.document("/markets/$marketId")
        _isLoading.postValue(true)
        firestore.stand()
                .whereEqualTo("market", marketRef)
                .get().addOnCompleteListener { task ->
                    if(task.isSuccessful) {
                        val docs = task.result
                        if (docs != null) {
                            val stands = docs.documents.map { it.toStand() }
                            _listStands.postValue(stands)
                        } else {
                            _listStands.postValue(emptyList())
                        }
                    } else {
                        Log.e(TAG, "Error fetching stands")
                    }
                    _isLoading.postValue(false)
                }
    }
}