package com.droidjs.marketplaces.views.stand

import android.app.Application
import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import com.droidjs.marketplaces.firebase.models.toProduce
import com.droidjs.marketplaces.firebase.models.toStand
import com.droidjs.marketplaces.firebase.standDoc
import com.droidjs.marketplaces.models.Produce
import com.droidjs.marketplaces.models.Stand
import com.droidjs.marketplaces.utils.Constants
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.FirebaseAnalytics.Event
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore

class StandViewModel(application: Application): AndroidViewModel(application) {

    private val analytics = FirebaseAnalytics.getInstance(application)
    private val firestore = FirebaseFirestore.getInstance()

    private val _stand = MutableLiveData<Stand>()
    val stand: LiveData<Stand> = _stand
    private val _isStandLoading = MutableLiveData<Boolean>()
    val isStandLoading: LiveData<Boolean> = _isStandLoading

    private val _produceList = MutableLiveData<List<Produce>>()
    val produceList: LiveData<List<Produce>> = _produceList

    fun findStand(uid: String) {
        if (_stand.value != null) return
        val doc = firestore.standDoc(uid)
        _isStandLoading.postValue(true)
        doc.get().addOnCompleteListener{ taskStand ->
            if(taskStand.isSuccessful){
                val document = taskStand.result
                Log.d("data",document?.data.toString())
                val stand = document?.toStand() as Stand
                val analyticsBundle = Bundle().apply {
                    putString(FirebaseAnalytics.Param.CONTENT_TYPE, Constants.ANALYTICS_CONTENT_TYPE_STAND)
                    putString(FirebaseAnalytics.Param.ITEM_ID, stand.uid)
                    putString(FirebaseAnalytics.Param.ITEM_NAME, stand.name)
                }
                analytics.logEvent(Event.SELECT_CONTENT, analyticsBundle)
                _stand.postValue(stand)
            }
            _isStandLoading.postValue(false)
        }
        findStandProducts(uid)
    }

    private fun findStandProducts(uid: String) {

        val standProduce = mutableListOf<Produce>()

        firestore.collection("productInStand")
                .whereEqualTo("stand", firestore.standDoc(uid))
                .whereEqualTo("isEnabled", true)
                .get()
                .addOnCompleteListener { task ->
                    if(task.isSuccessful) {
                        val productsInStand = task.result
                        productsInStand?.let {
                            for (doc: DocumentSnapshot in it.documents) {
                                val prodRef = doc.get("product") as DocumentReference
                                prodRef.get().addOnCompleteListener{ prodQuery ->
                                    if(prodQuery.isSuccessful){
                                        val prDocument = prodQuery.result
                                        val pr = prDocument!!.toProduce()
                                        standProduce.add(pr)
                                        _produceList.postValue(standProduce)
                                    }
                                }
                            }
                        }
                    }
                }
    }
}