package com.droidjs.marketplaces.views

import android.app.SearchManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.droidjs.marketplaces.R
import com.droidjs.marketplaces.utils.Constants.ANALYTICS_CONTENT_TYPE_MARKET
import com.droidjs.marketplaces.utils.Constants.NAV_LISTS
import com.droidjs.marketplaces.utils.Constants.NAV_MARKETS
import com.droidjs.marketplaces.utils.Constants.NAV_PRODUCE
import com.droidjs.marketplaces.views.auth.login.LoginActivity
import com.droidjs.marketplaces.views.favorites.FavoriteFragment
import com.droidjs.marketplaces.views.marketList.MarketListFragment
import com.droidjs.marketplaces.views.markets.MarketsMapFragment
import com.droidjs.marketplaces.views.produce.ProduceMonthPagerFragment
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.navigation

class MainActivity : AppCompatActivity(), FirebaseAuth.AuthStateListener {

    companion object {
        const val TAG = "MainActivity"
        const val QUERY_TYPE = "query_type"
        const val CURRENT_FRAGMENT = "active_fragment"
    }

    private lateinit var analytics: FirebaseAnalytics
    private val auth = FirebaseAuth.getInstance()

    private lateinit var navigationViewModel: NavigationViewModel
    private lateinit var currentFragment: Fragment

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_markets -> {
                navigationViewModel.selectNav(NAV_MARKETS)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_produce -> {
                navigationViewModel.selectNav(NAV_PRODUCE)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_lists -> {
                navigationViewModel.selectNav(NAV_LISTS)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        analytics = FirebaseAnalytics.getInstance(this)

        if (savedInstanceState == null) {
            val fragment = MarketsMapFragment()
            replaceNavFragment(fragment)
        }

        navigationViewModel = ViewModelProviders.of(this).get(NavigationViewModel::class.java)
        navigationViewModel.selectedBottomNavigation.observe(this, Observer { goTo ->
            navigateTo(goTo)
        })

        supportFragmentManager.addOnBackStackChangedListener { shouldDisplayHomeUp() }

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        handleIntent(intent)
    }

    override fun onStart() {
        super.onStart()
        auth.addAuthStateListener(this)
    }

    override fun onStop() {
        super.onStop()
        auth.removeAuthStateListener(this)
    }

    override fun onAuthStateChanged(auth: FirebaseAuth) {
        invalidateOptionsMenu()
    }

    private fun shouldDisplayHomeUp() {
        val canBack = supportFragmentManager.backStackEntryCount > 0
        supportActionBar?.setDisplayHomeAsUpEnabled(canBack)
    }

    private fun navigateTo(goTo: String?) {
        Log.d(TAG, "Navigating to $goTo")
        clearBackstack()
        when (goTo) {
            NAV_MARKETS -> {
                replaceNavFragment(MarketsMapFragment())
            }
            NAV_PRODUCE -> {
                replaceNavFragment(ProduceMonthPagerFragment())
            }
            NAV_LISTS -> {
                replaceNavFragment(FavoriteFragment())
            }
            else -> {
                Log.e(TAG, "Invalid navigation")
            }
        }
    }

    private fun clearBackstack() {
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    override fun onNewIntent(intent: Intent) {
        //setIntent(intent)
        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            intent.getStringExtra(SearchManager.QUERY)?.also { query ->
                val queryType = intent.getStringExtra(QUERY_TYPE).orEmpty()
                when (queryType) {
                    NAV_MARKETS -> {
                        val analyticsBundle = Bundle().apply {
                            putString(FirebaseAnalytics.Param.SEARCH_TERM, query)
                            putString(FirebaseAnalytics.Param.CONTENT_TYPE, ANALYTICS_CONTENT_TYPE_MARKET)
                        }
                        analytics.logEvent(FirebaseAnalytics.Event.SEARCH, analyticsBundle)
                        val fragment = MarketListFragment.newInstance(query)
                        replaceNavFragment(fragment, true)
                    }
                    else -> Log.e(TAG, "Invalid query type")
                }

            }
        }
    }

    private fun replaceNavFragment(fragment: androidx.fragment.app.Fragment, addInBackstack: Boolean = false, stateName: String? = null) {
        currentFragment = fragment
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment, CURRENT_FRAGMENT)
        if (addInBackstack) transaction.addToBackStack(stateName)
        transaction.commit()
    }

    override fun startActivity(intent: Intent?) {
        if (intent?.action?.equals(Intent.ACTION_SEARCH) == true) {
            val selectedNav = navigationViewModel.selectedBottomNavigation.value
            when (selectedNav ?: NAV_MARKETS) {
                NAV_MARKETS -> {
                    intent.putExtra(QUERY_TYPE, NAV_MARKETS)
                }
            }
        }
        super.startActivity(intent)
    }

    override fun onSupportNavigateUp(): Boolean {
        supportFragmentManager.popBackStack()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuResId = if (auth.currentUser != null) R.menu.main_logged_in_menu else R.menu.main_logged_out_menu
        menuInflater.inflate(menuResId, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        return when (id) {
            R.id.action_log_in -> {
                openLogIn()
                true
            }
            R.id.action_log_out -> {
                logout()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun openLogIn() {
        val loginIntent = Intent(this, LoginActivity::class.java)
        startActivity(loginIntent)
    }

    private fun logout() {
        auth.signOut()
    }
}
