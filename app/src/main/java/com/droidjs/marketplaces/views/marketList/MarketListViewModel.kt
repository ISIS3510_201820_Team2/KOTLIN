package com.droidjs.marketplaces.views.marketList

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import android.util.Log
import com.droidjs.marketplaces.firebase.market
import com.droidjs.marketplaces.firebase.models.toMarket
import com.droidjs.marketplaces.models.Market
import com.google.firebase.firestore.FirebaseFirestore

class MarketListViewModel(application: Application) : AndroidViewModel(application) {

    companion object {
        const val TAG = "MarketListViewModel"
    }

    val firestore: FirebaseFirestore = FirebaseFirestore.getInstance()

    val marketsList = MutableLiveData<List<Market>>()
    val isSearching = MutableLiveData<Boolean>().apply { postValue(false) }

    fun searchMarkets(query: String) {
        isSearching.postValue(true)
        firestore.market().get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val markets: List<Market> = task.result?.documents?.map {
                    doc -> doc.toMarket()
                }.orEmpty().filter { it.name.toLowerCase().contains(query.toLowerCase()) }
                marketsList.postValue(markets)
                isSearching.postValue(false)
            } else {
                Log.e(TAG, "Error fetching markets")
                isSearching.postValue(false)
            }
        }
    }
}