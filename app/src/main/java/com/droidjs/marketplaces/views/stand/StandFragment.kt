package com.droidjs.marketplaces.views.stand

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.droidjs.marketplaces.GlideApp
import com.droidjs.marketplaces.R
import com.droidjs.marketplaces.databinding.FragmentStandBinding
import com.droidjs.marketplaces.firebase.models.toUser
import com.droidjs.marketplaces.firebase.userDoc
import com.droidjs.marketplaces.models.Produce
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_stand.VendorImage
import kotlinx.android.synthetic.main.fragment_stand.empty_view_gv
import kotlinx.android.synthetic.main.fragment_stand.fav
import kotlinx.android.synthetic.main.fragment_stand.gvProduces
import kotlinx.android.synthetic.main.produce_entry.view.imgProduce

class StandFragment : Fragment() {
    companion object {
        const val KEY_STAND_UID = "key_stand"
        fun newInstance(marketUID: String): StandFragment {
            val args = Bundle().apply {
                putString(KEY_STAND_UID, marketUID)
            }
            return StandFragment().apply {
                arguments = args
            }
        }
    }

    private val firestore = FirebaseFirestore.getInstance()
    private val auth = FirebaseAuth.getInstance()
    private var adapter: ProduceAdapter? = null

    private lateinit var viewModel: StandViewModel
    private lateinit var binding: FragmentStandBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentStandBinding.inflate(inflater, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(StandViewModel::class.java)

        viewModel.stand.observe(this, Observer { stand ->
            GlideApp.with(this).load(stand?.imageUrl).into(VendorImage)
            adapter = ProduceAdapter(requireContext(), mutableListOf(), stand?.imageUrl.orEmpty())
            gvProduces.adapter = adapter
        })

        viewModel.produceList.observe(this, Observer { produceList ->
            if (produceList != null && produceList.isNotEmpty()) {
                adapter?.updateProduce(produceList)
                empty_view_gv.visibility = View.GONE
                gvProduces.visibility = View.VISIBLE
            }
        })
        binding.isStandLoading = viewModel.isStandLoading
        binding.stand = viewModel.stand
        viewModel.findStand(arguments?.getString(KEY_STAND_UID).orEmpty())

        isFavorite()

    }



    private fun isFavorite(){
        val currentUser = auth.currentUser
        if(currentUser != null) {
            fav.visibility = View.VISIBLE
            val docRef = firestore.userDoc(currentUser.uid)
            docRef.get().addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val user = task.result?.toUser()
                    if(user != null) {
                        val favorites = user.favorites.stands
                        val alreadyFavorited = favorites.contains(arguments?.getString(KEY_STAND_UID))
                        setFavoriteButton(alreadyFavorited, currentUser.uid)
                    }
                }
            }
        } else {
            fav.visibility = View.GONE
        }
    }

    private fun addFavorite(userId: String){
        val docRef = firestore.userDoc(userId)
        docRef.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val user = task.result?.toUser()
                if (user != null) {
                    val favorites = user.favorites.stands
                    val newStandId = arguments?.getString(KEY_STAND_UID).orEmpty()
                    if (!favorites.contains(newStandId)) {
                        docRef.update(
                                "favorites.stands", FieldValue.arrayUnion(newStandId)
                        )
                        setFavoriteButton(true, userId)
                    }
                }
            }
        }
    }

    private fun deleteFavorite(userId: String){
        val docRef = firestore.userDoc(userId)
        docRef.get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val user = task.result?.toUser()
                if(user != null) {
                    val favorites = user.favorites.stands
                    val standId = arguments?.getString(KEY_STAND_UID).orEmpty()
                    if (favorites.contains(standId)) {
                        docRef.update(
                                "favorites.stands", FieldValue.arrayRemove(standId)
                        )
                        setFavoriteButton(false, userId)
                    }
                }
            }
        }
    }

    private fun setFavoriteButton(alreadyFavorited: Boolean, userId: String) {
        if(alreadyFavorited) {
            fav?.setImageResource(R.drawable.ic_favorite_black_24dp)
            fav?.setOnClickListener { deleteFavorite(userId) }
        } else {
            fav?.setImageResource(R.drawable.ic_favorite_border_black_24dp)
            fav?.setOnClickListener { addFavorite(userId) }
        }
    }


    private inner class ProduceAdapter(context: Context, var produceList: MutableList<Produce>,
                                       val standImage: String) : BaseAdapter() {
        var context: Context? = context

        override fun getCount(): Int {
            return produceList.size
        }

        override fun getItem(position: Int): Produce {
            return produceList[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val produce = this.produceList[position]

            val inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val produceView = convertView ?: inflator.inflate(R.layout.produce_entry, null)

            produceView.imgProduce.setOnClickListener {

                val produceDetails = ProduceDetails.newInstance(uid = produce.uid,
                        image = produce.image, name = produce.name,
                        description = produce.description, standImageURL = standImage)
                activity?.supportFragmentManager?.beginTransaction()
                        ?.addToBackStack(null)
                        ?.replace(R.id.container, produceDetails)
                        ?.commit()
            }

            GlideApp.with(this@StandFragment).load(produce.image).into(produceView.imgProduce)
            return produceView
        }

        fun updateProduce(newProduce: List<Produce>) {
            produceList.clear()
            produceList.addAll(newProduce)
            notifyDataSetChanged()
        }
    }
}