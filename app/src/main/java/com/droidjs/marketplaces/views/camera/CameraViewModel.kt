package com.droidjs.marketplaces.views.camera

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.graphics.Bitmap
import android.util.Log
import com.droidjs.marketplaces.utils.Event
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage

class CameraViewModel: ViewModel() {

    companion object {
        const val TAG = "CameraViewModel"
        const val QR_REQUEST = "qr_request"
    }

    private val _fireCamera = MutableLiveData<Event<Boolean>>()
    val fireCamera: LiveData<Event<Boolean>> = _fireCamera

    private val _finishedProcessing = MutableLiveData<Event<String>>()
    val finishedProcessing: LiveData<Event<String>> = _finishedProcessing

    fun shootCamera() {
        _fireCamera.postValue(Event(true))
    }

    fun processBitmap(requestType: String, bitmap: Bitmap) {
        val textRecognizer = FirebaseVision.getInstance().onDeviceTextRecognizer
        textRecognizer.processImage(FirebaseVisionImage.fromBitmap(bitmap)).addOnSuccessListener { text -> Log.d(TAG, text.text) }
        when(requestType) {
            QR_REQUEST -> getQRInfo(bitmap)
            else -> _finishedProcessing.postValue(Event(""))
        }
    }

    private fun getQRInfo(bitmap: Bitmap) {
        val detector = FirebaseVision.getInstance().visionBarcodeDetector
        val image = FirebaseVisionImage.fromBitmap(bitmap)
        detector.detectInImage(image)
                .addOnSuccessListener {
                    val barcode = it.firstOrNull()
                    _finishedProcessing.postValue(Event(barcode?.rawValue.orEmpty()))
                }.addOnFailureListener {
                    _finishedProcessing.postValue(Event("ERROR"))
                }
    }

}