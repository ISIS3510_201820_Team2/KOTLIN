package com.droidjs.marketplaces.views.bargain

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class BargainViewModel : ViewModel() {

    private var state = BargainViewData()

    private val _data:MutableLiveData<BargainViewData> = MutableLiveData()
    val data: LiveData<BargainViewData> = _data

    fun initView(initialPrice: Double) {
        state = BargainViewData(initialPrice)
        _data.postValue(state)
    }

    fun setRecognizedText(text: String) {

        state = if(state.isUserTurn) {
            state.copy(userOffer = text)
        } else {
            state.copy(merchantOffer = text)
        }
        _data.postValue(state)
    }

    fun makeOffer(price: Double) {
        val turn = !state.isUserTurn
        state = state.copy(price = price, isUserTurn = turn)
        _data.postValue(state)
    }


    fun finishByMerchant() {
        state = state.copy(isUserTurn = true, merchantBlocked = true)
        _data.postValue(state)
    }

    fun finishByUser() {
        state = state.copy(userAgreed = true)
        _data.postValue(state)
    }
}

data class BargainViewData(
        val price: Double = 0.0,
        val userOffer: String = "",
        val merchantOffer: String = "",
        val isUserTurn: Boolean = true,
        val merchantBlocked: Boolean = false,
        val userAgreed: Boolean = false
)
