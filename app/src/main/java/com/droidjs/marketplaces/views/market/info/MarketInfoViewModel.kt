package com.droidjs.marketplaces.views.market.info

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import android.util.Log
import com.droidjs.marketplaces.firebase.marketDoc
import com.droidjs.marketplaces.firebase.models.toMarket
import com.droidjs.marketplaces.models.Market
import com.droidjs.marketplaces.views.market.MarketViewModel
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

class MarketInfoViewModel: ViewModel() {

    companion object {
        const val TAG  = "MarketInfoViewModel"
    }

    private val firestore = FirebaseFirestore.getInstance()
    private val _market = MutableLiveData<Market>()
    private val _open = MutableLiveData<String>()
    private val _isLoading = MutableLiveData<Boolean>()

    val market: LiveData<Market> = _market
    val open: LiveData<String> = _open
    val isLoading: LiveData<Boolean> = _isLoading

    fun searchMarket(uid: String) {
        if(uid == "") {
            Log.e(MarketViewModel.TAG, "Invalid search")
            return
        }
        _isLoading.postValue(true)
        firestore.marketDoc(uid).get().addOnCompleteListener { task ->
            if(task.isSuccessful) {
                val doc = task.result
                if (doc != null) {
                    Log.d(MarketViewModel.TAG, "Found market with id ${doc.id}")
                    val foundMarket = doc.toMarket()
                    _market.postValue(foundMarket)
                    _open.postValue(getOpenMessage(foundMarket, Date()))
                } else {
                    Log.e(MarketViewModel.TAG, "No Market with that ID")
                }
            } else {
                Log.e(MarketViewModel.TAG, "Error fetching markets")
            }
            _isLoading.postValue(false)
        }
    }

    private fun getOpenMessage(market: Market, date: Date): String {

        val calendar = Calendar.getInstance()
        calendar.time = date

        val dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)
        val times = when(dayOfWeek) {
            Calendar.MONDAY -> market.openTimes.mon
            Calendar.TUESDAY -> market.openTimes.tue
            Calendar.WEDNESDAY -> market.openTimes.wed
            Calendar.THURSDAY -> market.openTimes.thu
            Calendar.FRIDAY -> market.openTimes.fri
            Calendar.SATURDAY -> market.openTimes.sat
            Calendar.SUNDAY -> market.openTimes.sun
            else -> "Closed"
        }
        return if(times.isNotEmpty()) times else "Closed"
    }
}