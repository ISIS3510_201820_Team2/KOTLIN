package com.droidjs.marketplaces

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class MarketplacesAppGlideModule: AppGlideModule()
