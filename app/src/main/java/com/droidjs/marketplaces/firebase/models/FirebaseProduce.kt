package com.droidjs.marketplaces.firebase.models

import com.droidjs.marketplaces.models.Produce
import com.google.firebase.firestore.DocumentSnapshot

data class FirebaseProduce(
        val name:String = "",
        val description:String = "",
        val image:String = "",
        val seasonMonths: List<Int> = emptyList()
)
fun FirebaseProduce.toProduce(uid: String) = Produce(uid, name, description, image, seasonMonths)
fun DocumentSnapshot.toProduce() = toObject(FirebaseProduce::class.java)!!.toProduce(id)