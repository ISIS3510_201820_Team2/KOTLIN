package com.droidjs.marketplaces.firebase

import com.droidjs.marketplaces.utils.Constants.FIREBASE_MARKETS
import com.droidjs.marketplaces.utils.Constants.FIREBASE_PRODUCTS
import com.droidjs.marketplaces.utils.Constants.FIREBASE_STANDS
import com.droidjs.marketplaces.utils.Constants.FIREBASE_USERS
import com.google.firebase.firestore.FirebaseFirestore

fun FirebaseFirestore.users() = collection(FIREBASE_USERS)
fun FirebaseFirestore.userDoc(uid: String) = users().document(uid)
fun FirebaseFirestore.market() = collection(FIREBASE_MARKETS)
fun FirebaseFirestore.marketDoc(uid: String) = market().document(uid)
fun FirebaseFirestore.stand() = collection(FIREBASE_STANDS)
fun FirebaseFirestore.standDoc(uid: String) = stand().document(uid)
fun FirebaseFirestore.products() = collection(FIREBASE_PRODUCTS)
fun FirebaseFirestore.productDoc(uid: String) = products().document(uid)