package com.droidjs.marketplaces.firebase.models

import com.droidjs.marketplaces.models.Favorites
import com.droidjs.marketplaces.models.User
import com.google.firebase.firestore.DocumentSnapshot

data class FirebaseUser(
        val email: String = "",
        val firstName: String = "",
        val lastName: String = "",
        val nationality: String = "",
        val language: String = "",
        val favorites: FirebaseFavorites = FirebaseFavorites()
)

data class FirebaseFavorites(
        val produces: ArrayList<String> = ArrayList(),
        val stands: ArrayList<String> = ArrayList()
)

fun FirebaseFavorites.toFavorites() = Favorites(produces, stands)
fun Favorites.toFirebaseFavorites() = FirebaseFavorites(ArrayList(produces), ArrayList(stands))

fun FirebaseUser.toUser(uid: String) = User(uid, email, firstName, lastName,
        nationality, language, favorites.toFavorites())
fun User.toFirebaseUser() = FirebaseUser(email, firstName, lastName,
        nationality, language, favorites.toFirebaseFavorites())
fun DocumentSnapshot.toUser() = toObject(FirebaseUser::class.java)!!.toUser(id)