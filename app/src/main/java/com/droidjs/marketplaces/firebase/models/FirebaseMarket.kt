package com.droidjs.marketplaces.firebase.models

import com.droidjs.marketplaces.models.Market
import com.droidjs.marketplaces.models.WeekTimes
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.GeoPoint

data class FirebaseWeekTimes (
        val mon: String = "",
        val tue: String = "",
        val wed: String = "",
        val thu: String = "",
        val fri: String = "",
        val sat: String = "",
        val sun: String = ""
)
fun WeekTimes.toFirebaseWeekTimes() = FirebaseWeekTimes(mon, tue, wed, thu, fri, sat, sun)
fun FirebaseWeekTimes.toWeekTimes() = WeekTimes(mon, tue, wed, thu, fri, sat, sun)

data class FirebaseMarket(
        val name: String = "",
        val geoLoc: GeoPoint = GeoPoint(0.0, 0.0),
        val stands: List<String> = emptyList(),
        val summary: String = "", val location: String = "",
        val openTimes: FirebaseWeekTimes = FirebaseWeekTimes(), val imageUrl: String = ""
)
fun FirebaseMarket.toMarket(uid: String) = Market(uid = uid, imageUrl = imageUrl,
        lat = geoLoc.latitude, long = geoLoc.longitude, location = location, name = name,
        openTimes = openTimes.toWeekTimes(), stands = stands, summary = summary)
fun Market.toFirebaseMarket() = FirebaseMarket(
        name, GeoPoint(lat, long), stands, summary, location,
        openTimes.toFirebaseWeekTimes(), imageUrl)
fun DocumentSnapshot.toMarket() = toObject(FirebaseMarket::class.java)!!.toMarket(id)