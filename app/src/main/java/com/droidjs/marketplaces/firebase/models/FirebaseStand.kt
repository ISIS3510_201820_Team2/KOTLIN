package com.droidjs.marketplaces.firebase.models

import com.droidjs.marketplaces.models.Stand
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot

data class FirebaseStand(
        val name: String = "",
        val number: Int = 0,
        val imageUrl: String = "",
        val behindTheStand: String = "",
        val produces: List<DocumentReference> = emptyList(),
        val sellers: List<String> = emptyList()
){
    lateinit var market: DocumentReference

}

fun FirebaseStand.toStand(uid: String) = Stand(uid, market, name, number, imageUrl, behindTheStand, produces, sellers)
fun DocumentSnapshot.toStand() = toObject(FirebaseStand::class.java)!!.toStand(id)
